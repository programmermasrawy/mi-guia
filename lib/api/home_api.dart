
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/comment_model.dart';
import 'package:miguia/data/image_model.dart';
import 'package:miguia/data/news_model.dart';
import 'package:miguia/data/product_model.dart';

class HomeApi{
  var _url = "https://copper-egypt.com/delegate/public/api/";

  Future<List<CategoryModel>> getHomeCategories() async{
    try {
      var response = await Dio().get(_url+"categories");
      print(response);
     return  List<CategoryModel>.from(response.data['data']!.map((data) => CategoryModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<List<NewsModel>> getHomeSliders() async{
    try {
      var response = await Dio().get(_url+"slider");
      print(response);
      return  List<NewsModel>.from(response.data['data']!.map((data) =>
          NewsModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }


  Future<List<CategoryModel>> getSubCategories({int? catId}) async{
    try {
      print(catId.toString());
      var response = await Dio().get(_url+"sub/category/$catId");
      print(response);
      return  List<CategoryModel>.from(response.data['data']!.map((data) => CategoryModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<List<ProductModel>> getCategoryProducts({int? id}) async{
    try {
      print(id.toString());
      var response = await Dio().get(_url+"category/products/$id");
      print(response);
      return  List<ProductModel>.from(response.data['data']!.map((data) => ProductModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<List<ImageModel>> getFolderImages({int? id}) async{
    try {
      print(id.toString());
      var response = await Dio().get(_url+"product/folder/$id");
      print(response);
      return  List<ImageModel>.from(response.data['data'][0]['image']!.map((data) => ImageModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<List<ProductModel>> getRelated({int? id}) async{
    try {
      print(id.toString());
      var response = await Dio().get(_url+"product/related/$id");
      print(response);
      return  List<ProductModel>.from(response.data['data']!.map((data) => ProductModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<ProductModel> getProductDetails({int? id}) async {
    try {
      print(id.toString());
      var response = await Dio().get(_url+ "show/product/$id");
      print(response);
      return  List<ProductModel>.from(response.data['data']!.map((data) => ProductModel.fromMap(data)))[0];
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<List<ProductModel>> getProductOffer({int? id}) async {
    try {
      print(id.toString());
      var response = await Dio().get(_url+ "offer/$id");
      print(response);
      return response.data['data']!=null && response.data['data'].toString()!="[]"?
      List<ProductModel>.from(response.data['data']!.map((data) => ProductModel.fromMap(data))):[];
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<List<CommentModel>> getComments({id}) async {
    try {
      print(id.toString());
      var response = await Dio().get(_url+ "comments/$id");
      print(response);
      return response.data['data']!=null && response.data['data'].toString()!="[]"?
      List<CommentModel>.from(response.data['data']!.map((data) => CommentModel.fromMap(data))):[];
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }
 Future<bool> sendComment({comment,id}) async {
    try {
      print(id.toString());
      var response = await Dio().post(_url+ "comments",data: {
        "faq_id":id,
        "comment":comment
      });
      print(response);
      if(response.statusCode == 200 || response.statusCode == 201)
      return true;
      else return false;
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<DetailsModel> getPlaceDetails({int? id}) async{
    try {
      print(id.toString());
      var response = await Dio().get(_url+ "product/place/$id");
      print(response);
      return  List<DetailsModel>.from(response.data['data']!.map((data) => DetailsModel.fromMap(data)))[0];
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }
}