
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/news_model.dart';
import 'package:miguia/data/question_model.dart';
import 'package:miguia/data/settings_model.dart';

class SettingsApi{
  var _url = "https://copper-egypt.com/delegate/public/api/";

  Future<List<NewsModel>> fetchNews() async{
    try {
      var response = await Dio().get(_url+"news");
      print(response);
     return  List<NewsModel>.from(response.data['data']!.map((data) => NewsModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }
  Future<List<QuestionModel>> fetchQuestions() async{
    try {
      var response = await Dio().get(_url + "faqs");
      print("mahmoud"+response.data.toString());
     return  List<QuestionModel>.from(response.data['data']!.map((data) => QuestionModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

 Future<SettingsModel> getSettings() async{
    try {
      var response = await Dio().get(_url + "about");
      print("mahmoud"+response.data.toString());
     return  List<SettingsModel>.from(response.data['data']!.map((data) =>
         SettingsModel.fromMap(data)))[0];
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

}