import 'package:dio/dio.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/product_model.dart';

class SearchApi {
  var _url = "https://copper-egypt.com/delegate/public/api/";

  Future<List<CategoryModel>> getAreasForSearch() async {
    try {
      var response = await Dio().get(_url + "area");
      print(response);
      return List<CategoryModel>.from(
          response.data['data']!.map((data) => CategoryModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }

  Future<List<ProductModel>> search(
      {String? catId: "", String? areaId: "", String? data: ""}) async {
    try {
      var response = await Dio()
          .get(_url + "search?data=$data&category_id=$catId&area_id=$areaId");
      print(response);
      return List<ProductModel>.from(
          response.data['data']!.map((data) => ProductModel.fromMap(data)));
    } catch (e) {
      print(e);
      throw e.toString();
    }
  }
}
