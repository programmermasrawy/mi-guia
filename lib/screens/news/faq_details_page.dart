import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/data/question_model.dart';

import '../../main.dart';
import 'comments_page.dart';

class FaqsDetailsPage extends StatefulWidget {
  final List<QuestionModel>? news;
  int index;

  FaqsDetailsPage({Key? key, this.news, this.index: 0}) : super(key: key);

  @override
  _FaqsDetailsPageState createState() => _FaqsDetailsPageState();
}

class _FaqsDetailsPageState extends State<FaqsDetailsPage> {
  late PageController _pageViewCtrl;

  @override
  void initState() {
    super.initState();
    _pageViewCtrl = PageController(initialPage: widget.index);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var _padding = EdgeInsets.all(16);
    const _kDuration = const Duration(milliseconds: 300);
    const _kCurve = Curves.ease;

    return Scaffold(
      appBar: AppBar(
          title: Text('qAndr'.tr),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Get.back(),
          )),
      body: PageView.builder(
        controller: _pageViewCtrl,
        itemCount: widget.news!.length,
        itemBuilder: (BuildContext context, int index) {
          return Stack(
            children: [
              Container(
                width: width,
                height: height,
                color: PrimaryColor,
                child: Column(
                  children: [
                    Container(
                      color: PrimaryColor,
                      width: width,
                      height: height * .37,
                      child: CachedNetworkImage(
                          imageUrl: widget.news![index].image!,
                          width: width,
                          height: height * .37,
                          placeholder: (context, url) => Container(
                              height: 50,
                              width: 50,
                              child: SizedBox(
                                  height: 50,
                                  width: 50,
                                  child: Center(
                                      child: Center(
                                          child:
                                              CircularProgressIndicator())))),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          fit: BoxFit.fill),
                    ),
                    Expanded(
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: kRadius*3, topRight: kRadius*3),
                        child: Container(
                          color: Colors.white,
                          width: width,
                          child: ListView(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 25,left: 16,right: 16,bottom: 16),
                                child: Text(
                                  widget.news![index].translation![IsArabic ? 0 : 1]
                                      .name!,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: PrimaryColor),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(16),
                                child: Text(
                                  widget.news![index].translation![IsArabic ? 0 : 1]
                                      .description!,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14,
                                    color: Colors.black87,
                                  ),
                                  textDirection: TextDirection.ltr,
                                  textAlign: TextAlign.start,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(16),
                                child: InkWell(
                                  onTap: ()=> Get.to(CommentsPage(faqId: widget.news![index].id!.toString())),
                                  child: Text('show_comments'.tr,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14,
                                      color: Colors.black87,
                                      decoration: TextDecoration.underline
                                    ),
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ),
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                  bottom: 20,
                  left: 20,
                  right: 20,
                  child: Align(
                    alignment: AlignmentDirectional.bottomEnd,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                            elevation: 4.0,
                            child: new Icon(Icons.arrow_back),
                            backgroundColor: PrimaryColor,
                            onPressed: () {
                              _pageViewCtrl.nextPage(
                                  curve: _kCurve, duration: _kDuration);
                            }),
                        SizedBox(width: 8),
                        FloatingActionButton(
                            elevation: 4.0,
                            child: new Icon(Icons.arrow_forward),
                            backgroundColor: PrimaryColor,
                            onPressed: () {
                              _pageViewCtrl.previousPage(
                                  curve: _kCurve, duration: _kDuration);
                            })
                      ],
                    ),
                  ))
            ],
          );
        },
      ),
    );
  }
}
