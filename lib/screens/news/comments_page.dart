import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/api/home_api.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/comment_model.dart';
import 'package:miguia/widgets/home/empty_widget.dart';

import '../../main.dart';

class CommentsPage extends StatefulWidget {
  String? faqId;

  CommentsPage({this.faqId});

  @override
  State<StatefulWidget> createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  var _commentCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).loadComments = true;
    setState(() {});
    HomeCubit.get(context).getComments(context, widget.faqId!);
  }

  @override
  Widget build(BuildContext context) {
    var basicRadius = kRadius * 3;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<HomeCubit, HomeState>(
            builder: (BuildContext context, state) {
          return ConditionalBuilder(
              condition: !HomeCubit.get(context).loadComments,
              builder: (BuildContext context) {
                return Column(
                  children: [
                    Expanded(
                      child: ListView.separated(
                          itemCount: HomeCubit.get(context).comments.length,
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                          itemBuilder: (context, index) {
                            var comment =
                                HomeCubit.get(context).comments[index];
                            return InkWell(
                              onTap: () {},
                              child: SizedBox(
                                width: width,
                                // height: height * .25,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16, vertical: 6),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        comment.comment!,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black),
                                      ),
                                      SizedBox(height: 8),
                                      Text(comment.added_at!,
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.grey.shade500)),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                    TextField(
                      controller: _commentCtrl,
                      decoration: InputDecoration(hintText: "put_comment".tr),
                    ),
                    TextButton(
                      onPressed: () async {
                        bool result = await HomeApi().sendComment(
                            comment: _commentCtrl.text, id: widget.faqId);
                        if (result) {
                          HomeCubit.get(context).comments.add(CommentModel(
                              comment: _commentCtrl.text, added_at: ""));
                        }
                        _commentCtrl.clear();
                        setState(() {});
                      },
                      child: Text("add_comment".tr,
                          style: TextStyle(color: Colors.white)),
                    )
                  ],
                );
              },
              fallback: (BuildContext context) {
                return Center(
                  child: Container(
                      child: SizedBox(
                          height: 50,
                          width: 50,
                          child: Center(child: CircularProgressIndicator()))),
                );
              });
        }));
  }
}
