import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:miguia/screens/products/product_page.dart';

import '../../main.dart';

class CategoryProductsPage extends StatefulWidget {
  final CategoryModel? category;
  final catIndex;

  const CategoryProductsPage({Key? key, this.category, this.catIndex})
      : super(key: key);

  @override
  _CategoryProductsPageState createState() => _CategoryProductsPageState();
}

class _CategoryProductsPageState extends State<CategoryProductsPage> {
  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).getCategoryProducts(context, widget.category!.id);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var _padding = EdgeInsets.all(16);
    /*24 is for notification bar on Android*/
    final double itemHeight = (height - kToolbarHeight - 24) / 5;
    final double itemWidth = width / 3;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category!.translations![IsArabic ? 0 : 1].name!),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Get.back(),
        ),
      ),
      body: Container(
        padding: _padding,
        child: BlocBuilder<HomeCubit, HomeState>(
            builder: (BuildContext context, state) {
          return ConditionalBuilder(
            condition: HomeCubit.get(context).products.isNotEmpty,
            builder: (BuildContext context) {
              return GridView.count(
                  childAspectRatio: (itemWidth / itemHeight),
                  crossAxisCount: 3,
                  children: List.generate(
                      HomeCubit.get(context).products.length, (index) {
                    var product = HomeCubit.get(context).products[index];
                    return InkWell(
                      onTap: () {
                        bool offer= false;
                        String temp = product.visible!.firstWhere((element)
                        => element.contains("offer"),orElse: ()=> "");
                        if(temp.isNotEmpty)
                          offer = true;
                        Get.to(
                            TabBarPage(
                              product: product,
                                showOffer: offer,
                                child: ProductPage(
                              product: product,
                            )),
                            preventDuplicates: false,
                            popGesture: true);
                      },
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            margin: EdgeInsets.all(6),
                            child: ClipOval(
                              child: Container(
                                color: Color(0xff3FA9F5),
                                child: Padding(
                                  padding: EdgeInsets.all(2),
                                  child: ClipOval(
                                    child: CachedNetworkImage(
                                      imageUrl: product.logo!,
                                      height: itemHeight / 1.5,
                                      placeholder: (context, url) => Container(
                                          height: 70,
                                          width: 50,
                                          child: SizedBox(
                                            height: 70,
                                              width: 50,
                                              child: Center(child:Center(child:CircularProgressIndicator())))),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 4),
                          Text(
                            product.translations![IsArabic ? 0 : 1].name!,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(color: Colors.black),textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    );
                  }));
            },
            fallback: (BuildContext context) {
              return Center(
                child: Container(
                                          child: SizedBox(
                                            height: 50,
                                              width: 50,
                                              child:
                                                  Center(child:CircularProgressIndicator()))),
              );
            },
          );
        }),
      ),
    );
  }
}
