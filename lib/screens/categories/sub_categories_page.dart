import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:miguia/screens/products/product_page.dart';

import '../../main.dart';

class SubCategoriesPage extends StatefulWidget {
  final CategoryModel? category;
  final catIndex;

  const SubCategoriesPage({Key? key, this.category, this.catIndex})
      : super(key: key);

  @override
  _SubCategoriesPageState createState() => _SubCategoriesPageState();
}

class _SubCategoriesPageState extends State<SubCategoriesPage> {
  int _currentIndex = -1;

  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).getSubCategory(context, widget.category!.id);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    Widget tabBuild({String? icon, String? title}) {
      return SizedBox(
        height: 110,
        width: 80,
        child: Tab(
          child: Card(
            child: Container(
              height: 110,
              width: 80,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CachedNetworkImage(
                    imageUrl: icon!,
                    width: 50,
                    height: 50,
                  ),
                  AutoSizeText(title!,
                    minFontSize: 12,
                    maxFontSize: 17,maxLines: 1,),
                ],
              ),
            ),
          ),
        ),
      );
    }

    List<Widget> buildTabs(List<CategoryModel> subcategories) {
      List<Widget> widgets = [];

      subcategories.forEach((element) {
        widgets.add(tabBuild(
            icon: element.image!,
            title: element.translations![IsArabic ? 0 : 1].name!));
      });
      return widgets;
    }

    List<Widget> buildProducts(List<CategoryModel> subcategories) {
      List<Widget> widgets = [];
      subcategories.forEach((element) {
        widgets.add(ListView.builder(
            itemBuilder: (context, index) {
              final product = element.products![index];
              return Card(
                  margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  child: InkWell(
                    onTap: () {
                      bool offer= false;
                      String temp = product.visible!.firstWhere((element)
                      => element.contains("offer"),orElse: ()=> "");
                      if(temp.isNotEmpty)
                        offer = true;
                      Get.to(
                          TabBarPage(
                              product: product,
                              showOffer: offer,
                              child: ProductPage(
                                product: product,
                              )),
                          preventDuplicates: false,
                          popGesture: true);
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                      child: Row(
                        // mainAxisSize: MainAxisSize.min,
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ClipOval(
                                child: CachedNetworkImage(
                                  imageUrl: product.logo!,
                                  width: 45,
                                  height: 45,
                                  placeholder: (context, url) => Container(
                                      height: 50,
                                      width: 50,
                                      child: SizedBox(
                                          height: 45,
                                          width: 45,
                                          child: Center(child:Center(child:CircularProgressIndicator())))),
                                  errorWidget: (context, url, error) => Container(
                                    color: Colors.grey,
                                    width: 45,
                                    height: 45,
                                  ),
                                )),
                            SizedBox(
                              width: 6,
                            ),
                            Text(product.translations![IsArabic ? 0 : 1].name!),
                            Expanded(child: SizedBox()),
                            Container(
                              // width: width *.22,
                              child: RatingBar.builder(
                                initialRating: 3,
                                minRating: 1,
                                itemSize: 15,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 6,
                            ),
                            Card(
                              color: Colors.black45,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(kRadius * 3)),
                              child: Padding(
                                padding: EdgeInsets.all(4.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(width: 4),
                                    Text('show'.tr,
                                        style: TextStyle(color: Colors.white)),
                                    SizedBox(width: 6),
                                    Icon(CupertinoIcons.eye, color: Colors.white),
                                    SizedBox(width: 4),
                                  ],
                                ),
                              ),
                            )
                          ]),
                    ),
                  ));
            },
            itemCount: element.products!.length));
      });
      return widgets;
    }

    List<Widget> buildAll(List<CategoryModel> subcategories) {
      List<Widget> widgets = [];
      List<ProductModel> products = [];

      subcategories.forEach((element) {
        products.addAll(element.products!);
      });

      subcategories.forEach((element) {
        widgets.add(ListView.builder(
            itemBuilder: (context, index) {
              final product = products[index];
              return Card(
                  margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  child: InkWell(
                    onTap: () {
                      bool offer= false;
                      String temp = product.visible!.firstWhere((element)
                      => element.contains("offer"),orElse: ()=> "");
                      if(temp.isNotEmpty)
                        offer = true;
                      Get.to(
                          TabBarPage(
                              product: product,
                              showOffer: offer,
                              child: ProductPage(
                                product: product,
                              )),
                          preventDuplicates: false,
                          popGesture: true);
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                      child: Row(
                        // mainAxisSize: MainAxisSize.min,
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ClipOval(
                                child: CachedNetworkImage(
                                  imageUrl: product.logo!,
                                  width: 45,
                                  height: 45,
                                  placeholder: (context, url) => Container(
                                      height: 50,
                                      width: 50,
                                      child: SizedBox(
                                          height: 45,
                                          width: 45,
                                          child: Center(child:Center(child:CircularProgressIndicator())))),
                                  errorWidget: (context, url, error) => Container(
                                    color: Colors.grey,
                                    width: 45,
                                    height: 45,
                                  ),
                                )),
                            SizedBox(
                              width: 6,
                            ),
                            Text(product.translations![IsArabic ? 0 : 1].name!),
                            Expanded(child: SizedBox()),
                            Container(
                              // width: width *.22,
                              child: RatingBar.builder(
                                initialRating: 3,
                                minRating: 1,
                                itemSize: 15,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 6,
                            ),
                            Card(
                              color: Colors.black45,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(kRadius * 3)),
                              child: Padding(
                                padding: EdgeInsets.all(4.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(width: 4),
                                    Text('show'.tr,
                                        style: TextStyle(color: Colors.white)),
                                    SizedBox(width: 6),
                                    Icon(CupertinoIcons.eye, color: Colors.white),
                                    SizedBox(width: 4),
                                  ],
                                ),
                              ),
                            )
                          ]),
                    ),
                  ));
            },
            itemCount: products.length));
      });
      return widgets;
    }

    return BlocBuilder<HomeCubit, HomeState>(
        builder: (BuildContext context, state) {
      return ConditionalBuilder(
        condition: HomeCubit.get(context).subCategories.isNotEmpty,
        builder: (BuildContext context) {
          return SafeArea(
              child: DefaultTabController(
            length: HomeCubit.get(context).subCategories.length,
            child: Scaffold(
                body: Column(
              children: [
                Container(
                  height: height * .3,
                  width: width,
                  child: Stack(
                    children: [
                      Image.asset("assets/images/product_background.png",
                          height: height * .3, width: width, fit: BoxFit.fill),
                      Positioned.fill(
                          child: Align(
                        alignment: Alignment.bottomCenter,
                        child: ClipOval(
                          child: CachedNetworkImage(
                            imageUrl: widget.category!.image!,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Container(
                                height: 50,
                                width: 50,
                                child: SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: Center(child:Center(child:CircularProgressIndicator())))),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                            width: width * .32,
                            height: height * .15,
                          ),
                        ),
                      )),
                      Positioned.fill(
                          child: Align(
                        alignment: AlignmentDirectional.topStart,
                        child: Padding(
                          padding: EdgeInsets.all(24),
                          child: IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: () => Get.back(),
                          ),
                        ),
                      ))
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  widget.category!.translations![IsArabic ? 0 : 1].name!,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 22),
                ),
                SizedBox(height: 25),
                // the tab bar with two items
                TabBar(
                    unselectedLabelColor: Colors.grey.shade300,
                    indicatorSize: TabBarIndicatorSize.label,
                    isScrollable: true,
                    // onTap: (index){
                    //   setState(() {
                    //     _currentIndex = 1;
                    //   });
                    // },
                    indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: PrimaryColor),
                    tabs: buildTabs(HomeCubit.get(context).subCategories)),
                // create widgets for each tab bar here
                Expanded(
                  child: TabBarView(
                      children:
                      // _currentIndex == -1? buildAll(HomeCubit.get(context).subCategories):
                      buildProducts(HomeCubit.get(context).subCategories)),
                ),
              ],
            )),
          ));
        },
        fallback: (BuildContext context) {
          return Center(
            child: Container(
                child: SizedBox(
                    height: 50, width: 50, child: Center(child:CircularProgressIndicator()))),
          );
        },
      );
    });
  }
}
