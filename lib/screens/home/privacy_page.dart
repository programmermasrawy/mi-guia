import 'package:flutter/material.dart';
import 'package:miguia/config/constants.dart';

class PrivacyPage extends StatelessWidget {
  String? privacy_policy;
   PrivacyPage(this.privacy_policy, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            Text(privacy_policy??"",style: TextStyle(
                color: PrimaryColor
            ),
              textAlign: TextAlign.start,),
          ],
        ),
      ),
    );
  }
}
