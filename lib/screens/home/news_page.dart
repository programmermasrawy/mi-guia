import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/settings/settings_cubit.dart';
import 'package:miguia/screens/news/news_details_page.dart';

import '../../main.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({Key? key}) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> with TickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();
    SettingsCubit.get(this.context).fetchNews();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var theme = Theme.of(context);
    var _padding = EdgeInsets.all(16);
    return Scaffold(
      appBar: AppBar(
        title: Text('news'.tr),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Get.back(),
        ),
      ),
      body: Container(
        padding: _padding,
        child: BlocBuilder<SettingsCubit, SettingsState>(
            builder: (BuildContext context, state) {
          return ConditionalBuilder(
            condition: SettingsCubit.get(context).news.isNotEmpty,
            builder: (BuildContext context) {
              return ListView.builder(
                  // itemExtent: height * .15,
                  itemCount: SettingsCubit.get(context).news.length,
                  itemBuilder: (context, index) {
                    final int count = SettingsCubit.get(context).news.length;
                    var news = SettingsCubit.get(context).news[index];

                    return Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: InkWell(
                        onTap: () {
                          Get.to(NewsDetailsPage(
                              news: SettingsCubit.get(context).news,index: index));
                        },
                        child: Card(
                            color: Colors.grey.shade200,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(kRadius),
                                side:
                                    BorderSide(color: PrimaryColor, width: 1)),
                            child: Container(
                              height: height * .142,
                              padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 16),
                              child: Row(
                                children: [
                                  ClipRRect(
                                      borderRadius: BorderRadius.all(kRadius),
                                      child: CachedNetworkImage(
                                          imageUrl: news.image!,
                                          width: width * .22,
                                          height: height * .12,
                                          placeholder: (context, url) => Container(
                                              height: 50,
                                              width: 50,
                                              child: SizedBox(
                                                  height: 50,
                                                  width: 50,
                                                  child: Center(
                                                      child: Center(
                                                          child:
                                                              CircularProgressIndicator())))),
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error),
                                          fit: BoxFit.fill)),
                                  SizedBox(width: 8),
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          alignment: AlignmentDirectional.topStart,
                                          child: Text(
                                            news.translation![IsArabic ? 0 : 1]
                                                .name!
                                                .toUpperCase(),
                                            maxLines: 2,
                                            style: theme.textTheme.bodyText1!
                                                .copyWith(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black),
                                            textAlign: TextAlign.start,
                                          ),
                                        ),

                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text('readMore'.tr,
                                                style: TextStyle(
                                                    decoration:
                                                        TextDecoration.underline,color: Colors.grey)),
                                            Text(news.created_at ?? "",style: TextStyle(color: Colors.grey),)
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      ),
                    );
                  });
            },
            fallback: (BuildContext context) {
              return Center(
                child: Container(
                    child: SizedBox(
                        height: 50,
                        width: 50,
                        child: Center(child: CircularProgressIndicator()))),
              );
            },
          );
        }),
      ),
    );
  }
}
