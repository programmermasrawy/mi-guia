import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/config/fab_bottom_app_bar.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/screens/home/news_page.dart';
import 'package:miguia/screens/home/questions_page.dart';
import 'package:miguia/screens/home/settings_page.dart';
import 'package:miguia/screens/offers/offers_page.dart';
import 'package:share/share.dart';

import '../home_screen.dart';

class TabBarPage extends StatefulWidget {
  Widget? child;
  bool? showOffer;
  ProductModel? product;
  int? currentIndex;

  TabBarPage(
      {Key? key,
      this.child,
      this.showOffer: false,
      this.product: null,
      this.currentIndex})
      : super(key: key);

  @override
  _TabBarPageState createState() => _TabBarPageState();
}

class _TabBarPageState extends State<TabBarPage> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    _buildBottomNav() {
      return Container(
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: PrimaryColor),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(30), topLeft: Radius.circular(30)),
            // boxShadow: [
            //   BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
            // ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30.0),
              topRight: Radius.circular(30.0),
            ),
            child: FABBottomAppBar(
              centerItemText: '',
              color: Colors.grey.shade500,
              selectedColor: PrimaryColor,
              // notchedShape: CircularNotchedRectangle(),
              onTabSelected: (index) {
                widget.child = null;
                if (index == 1) {
                  Share.share('check out my website https://example.com');
                } else {
                  _currentIndex = index;
                  setState(() {});
                }
              },
              items: [
                FABBottomAppBarItem(
                    iconData: Icons.home_outlined, text: 'home'.tr),
                FABBottomAppBarItem(
                    iconData: CupertinoIcons.share_solid, text: 'share'.tr),
                FABBottomAppBarItem(
                    iconData: CupertinoIcons.chat_bubble_2, text: 'qAndr'.tr),
                FABBottomAppBarItem(
                    iconData: Icons.settings, text: 'settings'.tr),
              ],
            ),
          ));
    }

    Widget _buildFab(BuildContext context) {
      final icons = [Icons.sms, Icons.mail, Icons.phone];
      return FloatingActionButton(
        onPressed: () {},
        backgroundColor: PrimaryColor,
        tooltip: 'Increment',
        child: Icon(Icons.settings),
        elevation: 2.0,
//      ),
      );
    }

    return Scaffold(
        bottomNavigationBar: _buildBottomNav(),
        body: Container(
          padding:EdgeInsets.only(bottom:35),
            child:widget.child != null
            ? widget.child
            : _currentIndex == 0
                ? HomeScreen()
                : _currentIndex == 3
                    ? SettingsPage()
                    : _currentIndex == 2
                        ? QuestionsPage()
                        : SizedBox()),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: InkWell(
            onTap: () {
              if (widget.child != null && widget.showOffer!) {
                Get.to(OffersPage(product: widget.product),popGesture: true,preventDuplicates: false);
              } else {
                Get.to(NewsPage(),popGesture: true,preventDuplicates: false);
              }
            },
            child: Image.asset(
              widget.child == null || !widget.showOffer!
                  ? 'assets/images/news.png'
                  : 'assets/images/offer.png',
              width: width * .18,
              height: height * .17,
            )));
  }
}
