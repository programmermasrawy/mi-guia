import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/api/settings_api.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/config/strings.dart';
import 'package:miguia/data/settings_model.dart';
import 'package:miguia/main.dart';
import 'package:miguia/screens/home/about_app.dart';
import 'package:miguia/screens/home/privacy_page.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:country_picker/country_picker.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var _notif = true;
  late SettingsModel settings;
  @override
  void initState() {
    super.initState();
    SettingsApi().getSettings().then((value) {
      setState((){
        settings = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    var _padding = EdgeInsets.symmetric(horizontal: 16, vertical: 4);
    var _containerPadding = EdgeInsets.symmetric(horizontal: 16, vertical: 10);
    return Scaffold(
      appBar: AppBar(
        title: Text('settings'.tr),
      ),
      body: Container(
          child: ListView(
        children: [
          Container(
            width: width,
            padding: _containerPadding,
            color: Colors.grey.shade300,
            child: Text('appsSettings'.tr),
          ),
          SizedBox(
            height: height * .06,
            child: ListTile(
              contentPadding: _padding,
              onTap: () {
                showLangDialog();
              },
              title: Text('language'.tr),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.arrow_forward_ios_sharp, color: Colors.grey)
                ],
              ),
            ),
          ),
          Divider(),
          SizedBox(
            height: height * .06,
            child: ListTile(
              contentPadding: _padding,
              onTap: () {
                showCountryPicker(
                  context: context,
                  showPhoneCode: false, // optional. Shows phone code before the country name.
                  onSelect: (Country country) {
                    print('Select country: ${country.displayName}');
                  },
                );
              },
              title: Text('country'.tr),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.arrow_forward_ios_sharp, color: Colors.grey)
                ],
              ),
            ),
          ),
          Divider(),
          SizedBox(
            height: height * .06,
            child: ListTile(
              contentPadding: _padding,
              onTap: () {},
              title: Text('notifications'.tr),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [Switch(value: _notif, onChanged: (_) {
                  setState(() {
                    _notif = _;
                  });
                })],
              ),
            ),
          ),
          Divider(),

          Container(
            width: width,
            padding: _containerPadding,
            color: Colors.grey.shade300,
            child: Text('other'.tr),
          ),
          SizedBox(
            height: height * .06,
            child: ListTile(
              contentPadding: _padding,
              onTap: () async {
                if(settings!=null){
                  Get.to(PrivacyPage(settings.data![IsArabic?0:1].privacy_policy),popGesture: true);
                }
              },
              title: Text('privacy'.tr, style: TextStyle(color: PrimaryColor)),
            ),
          ),
          Divider(),
          SizedBox(
            height: height * .06,
            child: ListTile(
              contentPadding: _padding,
              onTap: () async {
                if(settings!=null){
                  Get.to(AboutApp(settings.data![IsArabic?0:1].about_us),popGesture: true);
                }
              },
              title: Text('about_app'.tr, style: TextStyle(color: PrimaryColor)),
            ),
          ),
          Divider(),
          SizedBox(
            height: height * .06,
            child: ListTile(
              contentPadding: _padding,
              onTap: () async {
                if(settings!=null){
                  Get.to(PrivacyPage(settings.data![IsArabic?0:1].privacy_policy),popGesture: true);
                }
              },
              title: Text(
                'terms'.tr,
                style: TextStyle(color: PrimaryColor),
              ),
            ),
          ),
          Divider(),
        ],
      )),
    );
  }

  showLangDialog() {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Card(
              margin: EdgeInsets.all(26),
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12))),
              child: Container(
                height: MediaQuery.of(context).size.height * .3,
                child: ClassicGeneralDialogWidget(
                  titleText: 'language'.tr,
                  contentText: 'chooseLang'.tr,
                  positiveText: '',
                  negativeText: '',
                  actions: [
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                              onTap: () async {
                                final shared =
                                await SharedPreferences.getInstance();
                                shared.setString('lang', 'ar');
                                IsArabic = true;
                                Get.updateLocale(Locale("ar", "EG"));
                                Get.offAll(TabBarPage());
                              },
                              child: Text("العربية")),
                          SizedBox(width: 150),
                          InkWell(
                              onTap: () async {
                                final shared =
                                await SharedPreferences.getInstance();
                                shared.setString('lang', 'En');
                                IsArabic = false;
                                Get.updateLocale(Locale("en", "US"));
                                Get.offAll(TabBarPage());
                              },
                              child: Text("English")),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        );
      },
      animationType: DialogTransitionType.size,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }

}
