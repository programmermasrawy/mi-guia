import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/cubit/settings/settings_cubit.dart';
import 'package:miguia/screens/news/faq_details_page.dart';

import '../../main.dart';

class QuestionsPage extends StatefulWidget {
  const QuestionsPage({Key? key}) : super(key: key);

  @override
  _QuestionsPageState createState() => _QuestionsPageState();
}

class _QuestionsPageState extends State<QuestionsPage>
    with TickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();
    SettingsCubit.get(this.context).fetchQuestions();
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery
        .of(context)
        .size
        .width;
    var height = MediaQuery
        .of(context)
        .size
        .height;
    var theme = Theme.of(context);
    var _padding = EdgeInsets.all(16);
    return Scaffold(
      appBar: AppBar(
        title: Text('qAndr'.tr),
      ),
      body: BlocBuilder<SettingsCubit, SettingsState>(
          builder: (BuildContext context, state) {
            return ConditionalBuilder(
              condition: SettingsCubit
                  .get(context)
                  .questions
                  .isNotEmpty,
              builder: (BuildContext context) {
                return ListView.separated(
                  itemCount: SettingsCubit
                      .get(context)
                      .questions
                      .length,
                  itemBuilder: (context, index) {
                    final int count = SettingsCubit
                        .get(context)
                        .questions
                        .length;
                    var question = SettingsCubit
                        .get(context)
                        .questions[index];
                    animationController.forward();
                    return AnimatedBuilder(
                        animation: animationController,
                        builder: (BuildContext? context, Widget? child) {
                          return AnimatedBuilder(
                              animation: animationController,
                              builder: (BuildContext? context, Widget? child) {
                                return ListTile(
                                  onTap: () {
                                    Get.to(FaqsDetailsPage(
                                        news: SettingsCubit.get(context).questions,index: index));
                                  },
                                  title: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: [
                                      SizedBox(height: 10),
                                      Text(
                                          question
                                              .translation![IsArabic ? 0 : 1]
                                              .name!,
                                          style: theme.textTheme.bodyText1!
                                              .copyWith(
                                              fontSize: 19,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                      SizedBox(height: 10),
                                      Text(
                                          question.translation![IsArabic ? 0 : 1].description!,
                                          maxLines: 2,
                                          style: theme.textTheme.bodyText1!
                                              .copyWith(
                                              fontSize: 16,
                                              color: Colors.grey.shade400)),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                  trailing: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        question.created_at ?? "",
                                        style: TextStyle(
                                            color: Colors.grey.shade500,
                                            fontSize: 13),
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                              question.comment_count.toString(),
                                              style: theme.textTheme.bodyText1!
                                                  .copyWith(
                                                  fontSize: 14,
                                                  color: Colors.grey.shade400)),
                                          SizedBox(width: 5),
                                          Icon(CupertinoIcons.chat_bubble_text),
                                        ],
                                      ),
                                    ],
                                  ),
                                  leading: ClipOval(
                                    child: CachedNetworkImage(
                                        imageUrl: question.image!,
                                        width: width * .16,
                                        height: height * .18,
                                        placeholder: (context, url) =>
                                            Container(
                                                height: 50,
                                                width: 50,
                                                child: SizedBox(
                                                    height: 50,
                                                    width: 50,
                                                    child: Center(
                                                        child: Center(
                                                            child:
                                                            CircularProgressIndicator())))),
                                        errorWidget: (context, url, error) =>
                                            Container(
                                              height: 50,
                                              width: 50,
                                              color: Colors.grey,
                                            ),
                                        fit: BoxFit.fill),
                                  ),
                                );
                              });
                        });
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return Divider(thickness: 1);
                  },
                );
              },
              fallback: (BuildContext context) {
                return Center(
                  child: Container(
                      child: SizedBox(
                          height: 50,
                          width: 50,
                          child: Center(child: CircularProgressIndicator()))),
                );
              },
            );
          }),
    );
  }
}
