import 'package:flutter/material.dart';
import 'package:miguia/config/constants.dart';

class AboutApp extends StatelessWidget {
  String? about_us;
   AboutApp(this.about_us, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("mahmoud $about_us");
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            Text(about_us??"",style: TextStyle(
                color: PrimaryColor
            ),
              textAlign: TextAlign.start,),
          ],
        ),
      ),
    );
  }
}
