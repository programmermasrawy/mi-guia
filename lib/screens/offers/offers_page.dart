import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/main.dart';
import 'package:miguia/screens/offers/offers_details_page.dart';
import 'package:miguia/widgets/home/empty_widget.dart';

class OffersPage extends StatefulWidget {
  ProductModel? product;

  OffersPage({Key? key, this.product}) : super(key: key);

  @override
  _OffersPageState createState() => _OffersPageState();
}

class _OffersPageState extends State<OffersPage> {
  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).loadingOffers = true;
    setState(() {});
    HomeCubit.get(context).getProductOffers(context, widget.product!.id!);
  }

  @override
  Widget build(BuildContext context) {
    var basicRadius = kRadius * 3;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<HomeCubit, HomeState>(
            builder: (BuildContext context, state) {
          return ConditionalBuilder(
              condition: !HomeCubit.get(context).loadingOffers,
              builder: (BuildContext context) {
                return HomeCubit.get(context).offers.isNotEmpty
                    ? ListView.builder(
                        itemCount: HomeCubit.get(context).offers.length,
                        itemBuilder: (context, index) {
                          var offer = HomeCubit.get(context).offers[index];
                          return InkWell(
                            onTap: (){
                              Get.to(OffersDetailsPage(
                                  offers: HomeCubit.get(context).offers,index : index));
                            },
                            child: Card(
                              margin: EdgeInsets.all(16),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(basicRadius),
                                  side: BorderSide(color: Colors.grey)),
                              child: SizedBox(
                                width: width,
                                // height: height * .25,
                                child: Stack(
                                  children: [
                                    Positioned.fill(
                                        child: Align(
                                      alignment: AlignmentDirectional.topStart,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                            topRight: basicRadius * 6,
                                            topLeft: basicRadius * 3,
                                            bottomRight: IsArabic
                                                ? Radius.zero
                                                : basicRadius * 3,
                                            bottomLeft: IsArabic
                                                ? basicRadius * 3
                                                : Radius.zero),
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 8, vertical: 4),
                                          child: Text(
                                            "Special Offer".tr,
                                            style: TextStyle(color: Colors.white),
                                          ),
                                          color: Colors.red.shade600,
                                        ),
                                      ),
                                    )),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          top: 35,
                                          left: 16,
                                          right: 16,
                                          bottom: 20),
                                      child: Row(children: [
                                        ClipOval(
                                            child: CachedNetworkImage(
                                          imageUrl: offer.logo!,
                                          width: 65,
                                          height: 65,
                                          fit: BoxFit.fill,
                                          placeholder: (context, url) => Container(
                                              height: 50,
                                              width: 50,
                                              child: SizedBox(
                                                  height: 45,
                                                  width: 45,
                                                  child: Center(
                                                      child:
                                                          CircularProgressIndicator()))),
                                          errorWidget: (context, url, error) =>
                                              Container(
                                            color: Colors.grey,
                                            width: 45,
                                            height: 45,
                                          ),
                                        )),
                                        SizedBox(
                                          width: 6,
                                        ),
                                        Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              offer
                                                  .translations![IsArabic ? 0 : 1]
                                                  .name!,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black),
                                            ),
                                            SizedBox(height: 8),
                                            Text(
                                                offer
                                                    .translations![
                                                        IsArabic ? 0 : 1]
                                                    .description!,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.grey.shade500)),
                                          ],
                                        ),
                                        Expanded(child: SizedBox()),
                                        Container(
                                          // width: width *.22,
                                          child: RatingBar.builder(
                                            initialRating: 3,
                                            minRating: 1,
                                            itemSize: 15,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                          ),
                                        ),
                                        SizedBox(
                                          width: 6,
                                        ),
                                      ]),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        })
                    : EmptyWidget();
              },
              fallback: (BuildContext context) {
                return Center(
                  child: Container(
                      child: SizedBox(
                          height: 50,
                          width: 50,
                          child: Center(child: CircularProgressIndicator()))),
                );
              });
        }));
  }
}
