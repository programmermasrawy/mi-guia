import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/data/product_model.dart';

import '../../main.dart';

class OffersDetailsPage extends StatefulWidget {
  final List<ProductModel>? offers;
  int index;

  OffersDetailsPage({Key? key, this.offers, this.index: 0}) : super(key: key);

  @override
  _OffersDetailsPageState createState() => _OffersDetailsPageState();
}

class _OffersDetailsPageState extends State<OffersDetailsPage> {
  late PageController _pageViewCtrl;

  @override
  void initState() {
    super.initState();
    _pageViewCtrl = PageController(initialPage: widget.index);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var _padding = EdgeInsets.all(16);
    const _kDuration = const Duration(milliseconds: 300);
    const _kCurve = Curves.ease;

    return Scaffold(
      appBar: AppBar(
          title: Text(''),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Get.back(),
          )),
      body: PageView.builder(
        controller: _pageViewCtrl,
        physics: NeverScrollableScrollPhysics(),
        itemCount: widget.offers!.length,
        itemBuilder: (BuildContext context, int index) {
          return Stack(
            children: [
              Container(
                width: width,
                height: height,
                child: ListView(
                  children: [
                    Container(
                      height:height * .37,
                      child:   ClipRRect(
                        borderRadius: BorderRadius.only(
                            bottomLeft: kRadius * 2, bottomRight: kRadius * 2),
                        child: CachedNetworkImage(
                            imageUrl:widget.offers![index].logo!,
                            width: width,
                            height: height * .37,
                            placeholder: (context, url) => Container(
                                height: 50,
                                width: 50,
                                child: SizedBox(
                                    height: 50,
                                    width: 50,
                                    child:
                                    Center(child:Center(child:CircularProgressIndicator())))),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                            fit: BoxFit.fill),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(16),
                      child: Text(
                        widget.offers![index].translations![IsArabic ?0 : 1].name!,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: PrimaryColor),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(16),
                      child: Text(
                        widget.offers![index].translations![IsArabic ?0 : 1].description!,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Colors.black87,
                        ),
                        textDirection: TextDirection.ltr,
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ),
              Positioned.fill(
                  bottom: 20,
                  left: 20,
                  right: 20,
                  child: Align(
                    alignment: AlignmentDirectional.bottomEnd,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        FloatingActionButton(
                            elevation: 4.0,
                            child: new Icon(Icons.arrow_back),
                            backgroundColor: PrimaryColor,
                            onPressed: () {
                              _pageViewCtrl.nextPage(
                                  curve: _kCurve, duration: _kDuration);
                            }),
                        SizedBox(width: 8),
                        FloatingActionButton(
                            elevation: 4.0,
                            child: new Icon(Icons.arrow_forward),
                            backgroundColor: PrimaryColor,
                            onPressed: () {
                              _pageViewCtrl.previousPage(
                                  curve: _kCurve, duration: _kDuration);
                            })
                      ],
                    ),
                  ))
            ],
          );
        },
      ),
    );
  }
}
