import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:miguia/screens/products/tech_products_page.dart';
import 'package:miguia/screens/search/search_screen.dart';

import '../main.dart';
import 'categories/category_products_page.dart';
import 'categories/sub_categories_page.dart';
import 'menu/custom_drawer.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    load();
  }

  load() async {
    HomeCubit.get(context).getSliders(context);
    HomeCubit.get(context).getHomeCategories(context);
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var _padding = EdgeInsets.all(16);
    var _space = SizedBox(height: 20);
    var textStyle =
        Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.black87);

    catItemBuild(CategoryModel category) {
      return InkWell(
        onTap: () {
          if (category.sub!)
            Get.to(TabBarPage(child: SubCategoriesPage(category: category)),
                preventDuplicates: false, popGesture: true);
          else {
            if(category.identify! == "career")
            Get.to(TabBarPage(child: TechProductsPage(category: category)),
                preventDuplicates: false, popGesture: true);
            else Get.to(TabBarPage(child: CategoryProductsPage(category: category)),
                preventDuplicates: false, popGesture: true);
          }
        },
        child: GridTile(
            footer: Container(color: Colors.black, height: 1),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          child: CachedNetworkImage(
                              imageUrl: category.image!,
                              placeholder: (context, url) => Container(
                                  height: 50,
                                  width: 50,
                                  child: SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: Center(child:Center(child:CircularProgressIndicator())))),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error))),
                      SizedBox(height: 10),
                      Text(category.translations![IsArabic ? 0 : 1].name!,
                          style: textStyle),
                    ]),
              ),
            )),
      );
    }

    itemVBuild(index, CategoryModel category, BorderRadius radius) {
      return InkWell(
        onTap: () {
          if (category.sub!)
            Get.to(TabBarPage(child: SubCategoriesPage(category: category)),
                preventDuplicates: false, popGesture: true);
          else {
            if(category.identify! == "career")
              Get.to(TabBarPage(child: TechProductsPage(category: category)),
                  preventDuplicates: false, popGesture: true);
            else Get.to(TabBarPage(child: CategoryProductsPage(category: category)),
                preventDuplicates: false, popGesture: true);
          }
        },
        child: Card(
            color: PrimaryColor,
            shape: RoundedRectangleBorder(borderRadius: radius),
            child: Container(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: CachedNetworkImage(
                            imageUrl: category.image!,
                            placeholder: (context, url) => Container(
                                height: 50,
                                width: 50,
                                child: SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: Center(child:Center(child:CircularProgressIndicator())))),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error))),
                    SizedBox(height: 10),
                    Text(category.translations![IsArabic ? 0 : 1].name!,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(color: Colors.white)),
                  ]),
            )),
      );
    }

    BoxDecoration myBoxDecoration(
        int length, int index, int gridViewCrossAxisCount) {
      index++;
      return BoxDecoration(
        border: Border(
          left: BorderSide(
            //                   <--- left side
            color: index % gridViewCrossAxisCount != 0
                ? Colors.black45
                : Colors.transparent,
            width: 1.5,
          ),
          top: BorderSide(
            //                   <--- left side
            color: index > gridViewCrossAxisCount &&
                    (index == length - 1 || index == length - 2)
                ? Colors.black12
                : Colors.transparent,
            width: 1.5,
          ),
        ),
      );
    }

    return BlocBuilder<HomeCubit, HomeState>(
        builder: (BuildContext context, state) {
          return ConditionalBuilder(
            condition: HomeCubit.get(context).categories.isNotEmpty,
            builder: (BuildContext context) {
              return Scaffold(
        key: _scaffoldKey,
        drawer: Align(
          alignment: Alignment.topRight,
          child: ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(45),
                  bottomLeft: Radius.circular(45)),
              child: CustomDrawer(HomeCubit.get(context).categories)),
        ),
        appBar: AppBar(
          title: Image.asset(
            "assets/images/Ma guia.png",
            height: 60,
            width: width * .35,
          ),
          leading: IconButton(
            icon: Image.asset("assets/images/menu.png"),
            onPressed: () {
              _scaffoldKey.currentState!.openDrawer();
            },
          ),
          actions: [
            IconButton(
                onPressed: () {
                  Get.to(TabBarPage(child: SearchScreen()),
                      popGesture: true, preventDuplicates: false);
                },
                icon: Icon(Icons.search),
                iconSize: 20),
          ],
        ),
        body: Container(
          padding: _padding,
          child:  Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if (HomeCubit.get(context).banner.isNotEmpty)
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: Container(
                          height: 115,
                          width: width,
                          child: Swiper(
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 25),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.all(kRadius),
                                  child: CachedNetworkImage(
                                      imageUrl: HomeCubit.get(context)
                                          .banner[index]
                                          .image!,
                                      width: width * .9,
                                      fit: BoxFit.fill,
                                      placeholder: (context, url) => Container(
                                          height: 50,
                                          width: 50,
                                          child: SizedBox(
                                              height: 50,
                                              width: 50,
                                              child: Center(child:
                                                  Center(child:Center(child:CircularProgressIndicator()))))),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                      height: 115),
                                ),
                              );
                            },
                            indicatorLayout: PageIndicatorLayout.COLOR,
                            autoplay: true,
                            scrollDirection: Axis.horizontal,
                            itemHeight: height * .1,
                            itemWidth: width,
                            duration: 5000,
                            autoplayDelay: 5000,
                            itemCount: HomeCubit.get(context).banner.length,
                            pagination: SwiperPagination(
                                builder: new DotSwiperPaginationBuilder(
                                    color: Colors.grey.shade300,
                                    activeColor: PrimaryColor)),
                            // control: SwiperControl(),
                          ),
                        ),
                      ),
                    SizedBox(height:10),
                    if (HomeCubit.get(context).sliders.isNotEmpty)
                      Directionality(
                        textDirection: TextDirection.rtl,
                        child: Container(
                          height: 115,
                          width: width,
                          child: Swiper(
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                margin: EdgeInsets.only(bottom: 25),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.all(kRadius),
                                  child: CachedNetworkImage(
                                      imageUrl: HomeCubit.get(context)
                                          .sliders[index]
                                          .image!,
                                      width: width * .9,
                                      fit: BoxFit.fill,
                                      placeholder: (context, url) => Container(
                                          height: 50,
                                          width: 50,
                                          child: SizedBox(
                                              height: 50,
                                              width: 50,
                                              child: Center(child:
                                                  Center(child:Center(child:CircularProgressIndicator()))))),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                      height: 115),
                                ),
                              );
                            },
                            indicatorLayout: PageIndicatorLayout.COLOR,
                            autoplay: true,
                            duration: 5000,
                            autoplayDelay: 5000,
                            itemHeight: height * .1,
                            itemWidth: width,
                            itemCount: HomeCubit.get(context).sliders.length,
                            pagination: SwiperPagination(
                                builder: new DotSwiperPaginationBuilder(
                                    color: Colors.grey.shade300,
                                    activeColor: PrimaryColor)),
                            // control: SwiperControl(),
                          ),
                        ),
                      ),
                  SizedBox(height:10),
                    Text(
                      'services'.tr,
                      style: Theme.of(context).textTheme.headline2,
                    ),
                      SizedBox(height:10),
                    Expanded(
                      child: (HomeCubit.get(context).categories.length == 4)
                          ? Column(
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      Expanded(
                                          child: itemVBuild(
                                              0,
                                              HomeCubit.get(context)
                                                  .categories[0],
                                              BorderRadius.only(
                                                  topRight: kRadius * 4))),
                                      Expanded(
                                        child: itemVBuild(
                                            1,
                                            HomeCubit.get(context)
                                                .categories[1],
                                            BorderRadius.only(
                                                topLeft: kRadius * 4)),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: itemVBuild(
                                            2,
                                            HomeCubit.get(context)
                                                .categories[2],
                                            BorderRadius.only(
                                                bottomRight: kRadius * 4)),
                                      ),
                                      Expanded(
                                        child: itemVBuild(
                                            3,
                                            HomeCubit.get(context)
                                                .categories[3],
                                            BorderRadius.only(
                                                bottomLeft: kRadius * 4)),
                                      ),
                                    ],
                                  ),
                                ),
                                // SizedBox(height: 40)
                              ],
                            )
                          : SizedBox(
                              height: height * .6,
                              width: width,
                              child: GridView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  // childAspectRatio: (3 / 4),
                                ),
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    decoration: myBoxDecoration(
                                        HomeCubit.get(context)
                                            .categories
                                            .length,
                                        index,
                                        2),
                                    child: catItemBuild(HomeCubit.get(context)
                                        .categories[index]),
                                  );
                                },
                                itemCount:
                                    HomeCubit.get(context).categories.length,
                              ),
                            ),
                    ),
                  ],
                )));
              },
              fallback: (BuildContext context) {
                return Scaffold(
                  body: Center(
                    child: Container(
                        child: SizedBox(
                            height: 50,
                            width: 50,
                            child: Center(child:CircularProgressIndicator()))),
                  ),
                );
              },
            );
          });
  }
}
