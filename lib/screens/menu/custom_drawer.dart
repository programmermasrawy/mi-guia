import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/main.dart';
import 'package:miguia/screens/categories/category_products_page.dart';
import 'package:miguia/screens/categories/sub_categories_page.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:miguia/screens/products/tech_products_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomDrawer extends StatefulWidget {
  List<CategoryModel> categories;
   CustomDrawer(this.categories, {Key? key}) : super(key: key);

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
      width: width * .65,
      height: height * .85,
      color: PrimaryColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/logo.png',width: width *.3,
          height: height * .3,),
          ListTile(
            onTap: (){
              showLangDialog();
            },
            title: Text('language'.tr,
                style: TextStyle(color: Colors.white)),
            trailing: CachedNetworkImage(imageUrl: '',width: 35,
              height: 35,),
          ),
          Divider(),
          Expanded(
            child: ListView.separated(
                separatorBuilder: (context,index){
                  return Divider();
                },
                itemBuilder: (context,index){
              var cat = widget.categories[index];
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 4),
                child: ListTile(
                  onTap: (){
                    if (cat.sub!)
                      Get.to(TabBarPage(child: SubCategoriesPage(category: cat)),
                          preventDuplicates: false, popGesture: true);
                    else {
                      if(cat.identify! == "career")
                        Get.to(TabBarPage(child: TechProductsPage(category: cat)),
                            preventDuplicates: false, popGesture: true);
                      else Get.to(TabBarPage(child: CategoryProductsPage(category: cat)),
                          preventDuplicates: false, popGesture: true);
                    }
                  },
                  title: Text(cat.translations![IsArabic ? 0 : 1].name!,
                  style: TextStyle(color: Colors.white)),
                  trailing: CachedNetworkImage(imageUrl: cat.image!,width: 35,
                  height: 35,),
                ),
              );
            },
            itemCount: widget.categories.length),
          ),
        ],
      ),
    );



  }

  showLangDialog() {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Card(
              margin: EdgeInsets.all(26),
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12))),
              child: Container(
                height: MediaQuery.of(context).size.height * .3,
                child: ClassicGeneralDialogWidget(
                  titleText: 'language'.tr,
                  contentText: 'chooseLang'.tr,
                  positiveText: '',
                  negativeText: '',
                  actions: [
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                              onTap: () async {
                                final shared =
                                await SharedPreferences.getInstance();
                                shared.setString('lang', 'ar');
                                IsArabic = true;
                                Get.updateLocale(Locale("ar", "EG"));
                                Get.offAll(TabBarPage());
                              },
                              child: Text("العربية")),
                          SizedBox(width: 150),
                          InkWell(
                              onTap: () async {
                                final shared =
                                await SharedPreferences.getInstance();
                                shared.setString('lang', 'En');
                                IsArabic = false;
                                Get.updateLocale(Locale("en", "US"));
                                Get.offAll(TabBarPage());
                              },
                              child: Text("English")),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        );
      },
      animationType: DialogTransitionType.size,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }
}
