import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/product_model.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../main.dart';

class TechProductsPage extends StatefulWidget {
  final CategoryModel? category;

  const TechProductsPage({Key? key, this.category}) : super(key: key);

  @override
  _TechProductsPageState createState() => _TechProductsPageState();
}

class _TechProductsPageState extends State<TechProductsPage> {
  List<bool> _showCareer = [];

  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).getCategoryProducts(context, widget.category!.id);
  }
  openSocial(String url, String fallbackUrl) async {
    // Don't use canLaunch because of fbProtocolUrl (fb://)
    try {
      bool launched =
      await launch(url, forceSafariVC: false, forceWebView: false);
      if (!launched) {
        await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
      }
    } catch (e) {
      await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
    }
  }

  sendMail(String email) {
    final Uri emailLaunchUri = Uri(
      scheme: 'mailto',
      path: email,
    );

    launch(emailLaunchUri.toString());
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    buildCareer(product){
      return Container(
        width: width,
        height: 50,
        padding: EdgeInsets.symmetric(
            horizontal: 7, vertical: 4),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return SizedBox(
                width: width * .2,
                height: 30,
                child: Card(
                  color: Colors.blue,
                  child: Center(
                      child: AutoSizeText(
                        product
                            .career![index]
                            .translations![
                        IsArabic ? 0 : 1]
                            .name!,
                        style: TextStyle(
                            color: Colors.white),
                      )),
                ),
              );
            },
            itemCount: product.career!.length),
      );
    }


    void _callPhone(String phone) async {
      var url = "tel:$phone";
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        throw 'Could not launch $url';
      }
    }


    showBottomSheet({ProductModel? product}) {
      showMaterialModalBottomSheet(
        elevation: 8,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) => Card(
          color:Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft:  Radius.circular(kCardRadius * 6),topRight:  Radius.circular(kCardRadius * 6),)
          ),
          child: Container(
              height: height * .55,
              width:width,
              padding: EdgeInsets.all(16),
              child: Column(
                children: [
                  Row(
                    // mainAxisSize: MainAxisSize.min,
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ClipOval(
                            child: CachedNetworkImage(
                              imageUrl: product!.logo!,
                              width: 45,
                              height: 45,
                              placeholder: (context, url) => Container(
                                  height: 50,
                                  width: 50,
                                  child: SizedBox(
                                      height: 45,
                                      width: 45,
                                      child: Center(
                                          child:
                                          CircularProgressIndicator()))),
                              errorWidget: (context, url, error) =>
                                  Container(
                                    color: Colors.grey,
                                    width: 45,
                                    height: 45,
                                  ),
                            )),
                        SizedBox(
                          width: 12,
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(product
                                .translations![IsArabic ? 0 : 1]
                                .name!,style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.black
                            ),),
                            SizedBox(height: 8),
                            Text(product
                                .translations![IsArabic ? 0 : 1]
                                .description!,style: TextStyle(
                                fontSize: 14,
                                color: Colors.grey.shade500
                            )),
                          ],
                        ),
                        Expanded(child: SizedBox()),
                        Container(
                          // width: width *.22,
                          child: RatingBar.builder(
                            initialRating: 3,
                            minRating: 1,
                            itemSize: 15,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                      ]),
                  buildCareer(product),
                  Divider(),
                  InkWell(
                      onTap: () => openSocial(
                          product.website!, product.website!),
                      child: buildDetails('website'.tr, product.website!)),
                  SizedBox(height: 10),
                  InkWell(
                      onTap: () => sendMail(product.email!),
                      child: buildDetails('email'.tr, product.email!)),
                  SizedBox(height: 10),
                  InkWell(
                      onTap: () => _callPhone(product.phone!),
                      child: buildDetails('tel'.tr, product.phone!)),
                  SizedBox(height: 10),
                  InkWell(
                      onTap: () => openSocial(product.facebook!,product.facebook!),
                      child: buildDetails('facebook'.tr, product.facebook!)),
                  SizedBox(height: 10),
                  Container(
                    width: width *.2,
                    height: 45,
                    child: TextButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      child: Text("ok".tr,style: TextStyle(color:Colors.white),),
                    ),
                  )
                ],
              )),
        ),
      );
    }

    return BlocBuilder<HomeCubit, HomeState>(
        builder: (BuildContext context, state) {
      return ConditionalBuilder(
        condition: HomeCubit.get(context).products.isNotEmpty,
        builder: (BuildContext context) {
          if (_showCareer.isEmpty)
            HomeCubit.get(context).products.forEach((element) {
              _showCareer.add(false);
            });
          return SafeArea(
              child: DefaultTabController(
            length: HomeCubit.get(context).products.length,
            child: Scaffold(
                body: ListView(
              children: [
                Container(
                  height: height * .3,
                  width: width,
                  child: Stack(
                    children: [
                      Image.asset("assets/images/product_background.png",
                          height: height * .3, width: width, fit: BoxFit.fill),
                      Positioned.fill(
                          child: Align(
                        alignment: Alignment.bottomCenter,
                        child: ClipOval(
                          child: CachedNetworkImage(
                            imageUrl: widget.category!.image!,
                            fit: BoxFit.fill,
                            placeholder: (context, url) => Container(
                                height: 50,
                                width: 50,
                                child: SizedBox(
                                    height: 50,
                                    width: 50,
                                    child: Center(
                                        child: CircularProgressIndicator()))),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                            width: width * .32,
                            height: height * .15,
                          ),
                        ),
                      )),
                      Positioned.fill(
                          child: Align(
                        alignment: AlignmentDirectional.topStart,
                        child: Padding(
                          padding: EdgeInsets.all(24),
                          child: IconButton(
                            icon: Icon(Icons.arrow_back),
                            onPressed: () => Get.back(),
                          ),
                        ),
                      ))
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  widget.category!.translations![IsArabic ? 0 : 1].name!,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 22),
                ),
                SizedBox(height: 25),
                ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      final product = HomeCubit.get(context).products[index];
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Card(
                              margin:
                                  EdgeInsets.only(top: 8, left: 8, right: 8),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    _showCareer[index] = !_showCareer[index];
                                  });
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 4, vertical: 8),
                                  child: Row(
                                      children: [
                                        ClipOval(
                                            child: CachedNetworkImage(
                                          imageUrl: product.logo!,
                                          width: 45,
                                          height: 45,
                                          placeholder: (context, url) => Container(
                                              height: 50,
                                              width: 50,
                                              child: SizedBox(
                                                  height: 45,
                                                  width: 45,
                                                  child: Center(
                                                      child:
                                                          CircularProgressIndicator()))),
                                          errorWidget: (context, url, error) =>
                                              Container(
                                            color: Colors.grey,
                                            width: 45,
                                            height: 45,
                                          ),
                                        )),
                                        SizedBox(
                                          width: 6,
                                        ),
                                        Text(product
                                            .translations![IsArabic ? 0 : 1]
                                            .name!),
                                        Expanded(child: SizedBox()),
                                        Container(
                                          // width: width *.22,
                                          child: RatingBar.builder(
                                            initialRating: 3,
                                            minRating: 1,
                                            itemSize: 15,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {
                                              print(rating);
                                            },
                                          ),
                                        ),
                                        SizedBox(
                                          width: 6,
                                        ),
                                        InkWell(
                                          onTap: () {
                                            showBottomSheet(product: product);
                                          },
                                          child: Card(
                                            color: Colors.black45,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    kRadius * 3)),
                                            child: Padding(
                                              padding: EdgeInsets.all(4.0),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  SizedBox(width: 4),
                                                  Text('show'.tr,
                                                      style: TextStyle(
                                                          color: Colors.white)),
                                                  SizedBox(width: 6),
                                                  Icon(CupertinoIcons.eye,
                                                      color: Colors.white),
                                                  SizedBox(width: 4),
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ]),
                                ),
                              )),
                          Visibility(
                              visible: _showCareer[index],
                              child: Card(
                                margin: EdgeInsets.only(left: 16, right: 16),
                                color: Colors.grey,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        bottomRight: kRadius * 3,
                                        bottomLeft: kRadius * 3)),
                                child: buildCareer(product),
                              ))
                        ],
                      );
                    },
                    itemCount: HomeCubit.get(context).products.length)
              ],
            )),
          ));
        },
        fallback: (BuildContext context) {
          return Center(
            child: Container(
                child: SizedBox(
                    height: 50,
                    width: 50,
                    child: Center(child: CircularProgressIndicator()))),
          );
        },
      );
    });
  }

  buildDetails(title, subtitle) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(title,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: Colors.grey.shade600,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
          ),
          Text( ":",
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.grey.shade600,
                  fontSize: 16,
                  fontWeight: FontWeight.bold)),
          SizedBox(width: 10),
          Expanded(
            flex: 2,
            child: Text(subtitle,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.grey.shade600, fontSize: 16)),
          ),
        ],
      ),
    );
  }
}
