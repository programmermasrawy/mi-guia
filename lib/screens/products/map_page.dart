import 'dart:async';

import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:map_launcher/map_launcher.dart' as launcher;
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/widgets/home/empty_widget.dart';

import '../../main.dart';

class MapPage extends StatefulWidget {
  DetailsModel? place;

  MapPage({Key? key, this.place}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).generalLoading = true;
    HomeCubit.get(context).getPlaceDetails(context, widget.place!.id!);
  }

  Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          BlocBuilder<HomeCubit, HomeState>(
            builder: (BuildContext context, state) {
              return ConditionalBuilder(
                  condition: HomeCubit.get(context).generalLoading,
                  builder: (BuildContext context) {
                    if (HomeCubit.get(context).place != null) {
                      var place = HomeCubit.get(context).place;
                      print(place!.lat! + " && " + place.lng!);
                      double lat = double.tryParse(place.lat!)! > 90
                          ? 90
                          : double.tryParse(place.lat!)!;
                      LatLng? latLng =
                          LatLng(lat, double.tryParse(place.lng!)!);

                      return Container(
                          height: height,
                          child: Stack(
                            children: [
                              SizedBox(
                                height: height * .55,
                                width: width,
                                child: GoogleMap(
                                  mapType: MapType.normal,
                                  markers: {
                                    Marker(
                                        markerId: MarkerId("1"),
                                        position: latLng)
                                  },
                                  initialCameraPosition: CameraPosition(
                                    target: latLng,
                                    zoom: 9,
                                  ),
                                  onMapCreated:
                                      (GoogleMapController controller) {
                                    _controller.complete(controller);
                                  },
                                ),
                              ),
                              Positioned.fill(
                                child: Align(
                                  alignment: Alignment.bottomCenter,
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: kRadius * 4,
                                            topRight: kRadius * 4)),
                                    child: Container(
                                      height: height * .46,
                                      width: width,
                                      padding: EdgeInsets.all(24),
                                      child: Column(
                                        children: [
                                          Text(
                                              place
                                                  .translations![
                                                      IsArabic ? 0 : 1]
                                                  .name!,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: Colors.black,
                                                      fontSize: 22,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      decoration: TextDecoration
                                                          .underline)),
                                          SizedBox(height: 10),
                                          Text(place
                                              .translations![IsArabic ? 0 : 1]
                                              .description!),
                                          SizedBox(height: 10),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned.fill(
                                  // bottom: 60,
                                  child: Align(
                                alignment: Alignment.bottomCenter,
                                child: TextButton(
                                  onPressed: () async {
                                    final availableMaps = await launcher
                                        .MapLauncher.installedMaps;
                                    print(
                                        availableMaps); // [AvailableMap { mapName: Google Maps, mapType: google }, ...]

                                    await availableMaps.first.showMarker(
                                      coords: launcher.Coords(
                                          latLng.latitude, latLng.longitude),
                                      title: "",
                                    );
                                  },
                                  style: TextButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                              topLeft: kRadius * 3,
                                              topRight: kRadius * 3))),
                                  child: Text(
                                    'LOCATE A STORE'.tr.toUpperCase(),
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ))
                            ],
                          ));
                    } else {
                      return EmptyWidget();
                    }
                  },
                  fallback: (BuildContext context) {
                    return Center(
                      child: Container(
                          child: SizedBox(
                              height: 50,
                              width: 50,
                              child:
                                  Center(child: CircularProgressIndicator()))),
                    );
                  });
            },
          ),
          Positioned.fill(
              child: Align(
            alignment: AlignmentDirectional.topStart,
            child: Padding(
              padding: EdgeInsets.all(30),
              child: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Get.back(),
              ),
            ),
          ))
        ],
      )),
    );
  }
}
