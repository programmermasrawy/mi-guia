import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/main.dart';

class CarearPage extends StatefulWidget {
  final ProductModel? product;

  const CarearPage({Key? key, this.product}) : super(key: key);

  @override
  _CarearPageState createState() => _CarearPageState();
}

class _CarearPageState extends State<CarearPage> {


  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Container(
      // margin: EdgeInsets.symmetric(vertical: 4,horizontal: 16),
      width: width,
      child: ListView.builder(
          itemBuilder: (context, index) {
            final carear = widget.product!.career![index];
            return Card(
              margin: EdgeInsets.symmetric(horizontal: 16 , vertical: 4),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  // mainAxisSize: MainAxisSize.min,
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ClipOval(
                          child: CachedNetworkImage(
                            imageUrl: carear.photo!,
                            width: 45,
                            height: 45,
                            placeholder: (context, url) => Container(
                                height: 50,
                                width: 50,
                                child: SizedBox(
                                    height: 45,
                                    width: 45,
                                    child: Center(child:Center(child:CircularProgressIndicator())))),
                            errorWidget: (context, url, error) => Container(
                              color: Colors.grey,
                              width: 45,
                              height: 45,
                            ),
                          )),
                      SizedBox(
                        width: 6,
                      ),
                      Text(carear.translations![IsArabic ? 0 : 1].name!),
                      Expanded(child: SizedBox()),
                      Container(
                        // width: width *.22,
                        child: RatingBar.builder(
                          initialRating: 3,
                          minRating: 1,
                          itemSize: 15,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      Card(
                        color: Colors.black45,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(kRadius * 3)),
                        child: Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(width: 4),
                              Text('show'.tr, style: TextStyle(color: Colors.white)),
                              SizedBox(width: 6),
                              Icon(CupertinoIcons.eye, color: Colors.white),
                              SizedBox(width: 4),
                            ],
                          ),
                        ),
                      )
                    ]),
              ),
            );
          },
          itemCount: widget.product!.career!.length),
    );

  }

  @override
  void initState() {
    super.initState();
    print("mahmoudd");
  }
}
