import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/screens/folder_photos_screen/folder_photos_screen.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';

import '../../main.dart';

class PhotosPage extends StatefulWidget {
  final ProductModel? product;

  const PhotosPage({Key? key, this.product}) : super(key: key);

  @override
  _PhotosPageState createState() => _PhotosPageState();
}

class _PhotosPageState extends State<PhotosPage> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context).textTheme;
    return ListView.builder(
        itemBuilder: (context, index) {
          var folder = widget.product!.folder![index];
          return ListTile(
            onTap: () {
              Get.to(TabBarPage(child: FolderPhotosScreen(folder: folder)),
                  preventDuplicates: false, popGesture: true);
            },
            leading: ClipRRect(
              borderRadius: BorderRadius.all(kRadius * 2),
              child: CachedNetworkImage(
                imageUrl: folder.photo!,
                width: size.width * .2,
                placeholder: (context, url) => Container(
                    height: 50,
                    width: 50,
                    child: SizedBox(
                        height: 50,
                        width: 50,
                        child: Center(child:CircularProgressIndicator()))),
                errorWidget: (context, url, error) => Container(
                  color: Colors.grey,
                  width: size.width * .2,
                  height: size.height * .2,
                ),
                height: size.height * .2,
              ),
            ),
            title: Text(
              folder.translations![IsArabic ? 0 : 1].name!,
              maxLines: 2,
              style: theme.bodyText2,
            ),
            subtitle: Text(
              folder.translations![IsArabic ? 0 : 1].description!,
              style: theme.subtitle1,
              maxLines: 2,
            ),
            trailing: Icon(Icons.arrow_forward_ios_sharp),
          );
        },
        itemCount: widget.product!.folder!.length);
  }
}
