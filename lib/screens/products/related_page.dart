import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/main.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:miguia/screens/products/product_page.dart';

class RelatedPage extends StatefulWidget {
  final ProductModel? product;

  const RelatedPage({Key? key, this.product}) : super(key: key);

  @override
  _RelatedPageState createState() => _RelatedPageState();
}

class _RelatedPageState extends State<RelatedPage> {



  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    var _padding = EdgeInsets.all(16);
    /*24 is for notification bar on Android*/
    final double itemHeight = (height - kToolbarHeight - 24) / 5;
    final double itemWidth = width / 3;


   return Scaffold(
     appBar: AppBar(),
     body: BlocBuilder<HomeCubit, HomeState>(
          builder: (BuildContext context, state) {
            return ConditionalBuilder(
              condition: HomeCubit.get(context).related.isNotEmpty,
              builder: (BuildContext context) {
                final related = HomeCubit.get(context).related;

                return GridView.count(
                    childAspectRatio: (itemWidth / itemHeight),
                    crossAxisCount: 3,
                    children: List.generate(
                        related.length, (index) {
                      var product = related[index];
                      return InkWell(
                        onTap: () {
                          bool offer= false;
                          String temp = product.visible!.firstWhere((element)
                          => element.contains("offer"),orElse: ()=> "");
                          if(temp.isNotEmpty)
                            offer = true;
                          Get.to(
                              TabBarPage(
                                  product: product,
                                  showOffer: offer,
                                  child: ProductPage(
                                    product: product,
                                  )),
                              preventDuplicates: false,
                              popGesture: true);
                        },
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              margin: EdgeInsets.all(6),
                              child: ClipOval(
                                child: Container(
                                  color: Color(0xff3FA9F5),
                                  child: Padding(
                                    padding: EdgeInsets.all(2),
                                    child: ClipOval(
                                      child: CachedNetworkImage(
                                        imageUrl: product.logo!,
                                        height: itemHeight / 1.5,
                                        placeholder: (context, url) => Container(
                                            height: 70,
                                            width: 50,
                                            child: SizedBox(
                                                height: 70,
                                                width: 50,
                                                child: Center(child:Center(child:CircularProgressIndicator())))),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 4),
                            Text(
                              product.translations![IsArabic ? 0 : 1].name!,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(color: Colors.black),textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      );
                    }));
              },
              fallback: (BuildContext context) {
                return Center(
                  child: Container(
                      child: SizedBox(
                          height: 50,
                          width: 50,
                          child: Center(child:CircularProgressIndicator()))),
                );
              },
            );
          }),
   );
  }

  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).getRelated(context, widget.product!.id!);
  }
}
