import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/screens/products/carear_page.dart';
import 'package:miguia/screens/products/photos_page.dart';
import 'package:miguia/screens/products/places_page.dart';
import 'package:miguia/screens/products/related_page.dart';

import '../../main.dart';
import 'about_page.dart';

class ProductPage extends StatefulWidget {
  final ProductModel? product;

  const ProductPage({Key? key, this.product}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  bool places = true;
  bool folders = true;
  bool related = true;
  bool career = true;

  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).getProductDetails(context, widget.product!.id);


  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery
        .of(context)
        .size
        .width;
    var height = MediaQuery
        .of(context)
        .size
        .height;
    var _padding = EdgeInsets.all(16);

    Widget tabBuildRelated({IconData? icon, String? title,product}) {
      return InkWell(
        onTap: (){
          Get.to(RelatedPage(product: product));
        },
        child: SizedBox(
          height: 110,
          width: 80,
          child: Tab(
            child: Card(
              child: Container(
                height: 110,
                width: 80,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(icon!),
                    SizedBox(height:8),
                    AutoSizeText(title!,minFontSize: 12,maxFontSize: 17,
                        textAlign: TextAlign.center,style: TextStyle(fontSize: 12),),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        body: BlocBuilder<HomeCubit, HomeState>(
            builder: (BuildContext context, state) {
              return ConditionalBuilder(
                condition: HomeCubit
                    .get(context)
                    .productDetails != null,
                builder: (BuildContext context) {
                  final product = HomeCubit
                      .get(context)
                      .productDetails;
                  String temp = product!.visible!.firstWhere((element)
                  => element.contains("place"),orElse: ()=> "");
                  if(temp.isEmpty)
                    places = false;
                  String temp2 = product.visible!.firstWhere((element)
                  => element.contains("folder"),orElse: ()=> "");
                  if(temp2.isEmpty)
                    folders = false;
                  String temp3 = product.visible!.firstWhere((element)
                  => element.contains("related"),orElse: ()=> "");
                  if(temp3.isEmpty)
                    related = false;
                  String temp4 = product.visible!.firstWhere((element)
                  => element.contains("career"),orElse: ()=> "");
                  if(temp4.isEmpty)
                    career = false;
                  var length = 5 - (!career ? 1 : 0) - (!folders ? 1 : 0) - (!related ? 1 : 0) -(!places ? 1 : 0);
                  return SafeArea(
                      child: DefaultTabController(
                          length: length,
                          child: Scaffold(
                              body: Container(
                                width: width,
                                height: height,
                                child: Column(
                                  children: [
                                    Container(
                                      height: height * .3,
                                      width: width,
                                      child: Stack(
                                        children: [
                                          Image.asset(
                                              "assets/images/product_background.png",
                                              height: height * .3,
                                              width: width,
                                              fit: BoxFit.fill),
                                          Positioned.fill(
                                              child: Align(
                                                alignment: Alignment.bottomCenter,
                                                child: ClipOval(
                                                  child: CachedNetworkImage(
                                                    imageUrl: product.logo!,
                                                    fit: BoxFit.fill,
                                                    placeholder: (context, url) =>
                                                        Container(
                                                            height: 50,
                                                            width: 50,
                                                            child: SizedBox(
                                                                height: 50,
                                                                width: 50,
                                                                child:
                                                                Center(child:CircularProgressIndicator()))),
                                                    errorWidget: (context, url,
                                                        error) =>
                                                        Icon(Icons.error),
                                                    width: width * .32,
                                                    height: height * .15,
                                                  ),
                                                ),
                                              )),
                                          Positioned.fill(
                                              child: Align(
                                                alignment: AlignmentDirectional
                                                    .topStart,
                                                child: Padding(
                                                  padding: EdgeInsets.all(24),
                                                  child: IconButton(
                                                    icon: Icon(Icons.arrow_back),
                                                    onPressed: () => Get.back(),
                                                  ),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text(
                                      product.translations![IsArabic ? 0 : 1].name!,
                                      textAlign: TextAlign.center,
                                      style: Theme
                                          .of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                    ),
                                    SizedBox(height: 25),
                                    // the tab bar with two items
                                    TabBar(
                                      unselectedLabelColor: Colors.grey.shade300,
                                      indicatorSize: TabBarIndicatorSize.label,
                                      indicator: BoxDecoration(
                                          borderRadius: BorderRadius.circular(16),
                                          color: PrimaryColor),
                                      tabs: [
                                        if(places)tabBuild(
                                            icon: Icons.location_on_outlined,
                                            title: 'places'.tr),
                                       if(folders) tabBuild(
                                            icon: Icons.photo_album,
                                            title: 'photos'.tr),
                                        if (career)
                                          tabBuild(
                                              icon: Icons.location_on_outlined,
                                              title: 'career'.tr),
                                        tabBuild(
                                            icon: CupertinoIcons.waveform_path,
                                            title: 'about'.tr),
                                        if (related)
                                          tabBuildRelated(
                                              icon: FontAwesomeIcons.firstdraft,
                                              title: 'related'.tr,product: product),
                                      ],
                                    ),
                                    // create widgets for each tab bar here
                                    Expanded(
                                      child: TabBarView(
                                        children: [
                                          if(places)  PlacesPage(product: product),
                                          if(folders) PhotosPage(product: product),
                                          if (career) CarearPage(product: product),
                                          AboutPage(product: product),
                                          if (related) RelatedPage(product: product),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ))));
                },
                fallback: (BuildContext context) {
                  return Center(
                    child: Container(
                        child: SizedBox(
                            height: 50,
                            width: 50,
                            child: Center(child:CircularProgressIndicator()))),
                  );
                },
              );
            }),
      ),
    );
  }

  Widget tabBuild({IconData? icon, String? title}) {
    return SizedBox(
      height: 110,
      width: 80,
      child: Tab(
        child: Card(
          child: Container(
            height: 110,
            width: 80,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(icon!),
                SizedBox(height:8),
                AutoSizeText(title!),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
