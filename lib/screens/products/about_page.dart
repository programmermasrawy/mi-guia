import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/data/product_model.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../main.dart';

class AboutPage extends StatefulWidget {
  final ProductModel? product;

  const AboutPage({Key? key, this.product}) : super(key: key);

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var _padding = EdgeInsets.only(top:16,left:16,right:16,bottom: 40);

    return Container(
      padding: _padding,
      child: ListView(
        children: [
          Text(widget.product!.translations![IsArabic ? 0 : 1].name!,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold)),
          SizedBox(height: 15),
          Text(widget.product!.translations![IsArabic ? 0 : 1].description!,
              style: Theme.of(context).textTheme.subtitle1!),
          SizedBox(height: 30),
          InkWell(
              onTap: () => openSocial(
                  widget.product!.website!, widget.product!.website!),
              child: buildDetails('website'.tr, widget.product!.website!)),
          SizedBox(height: 10),
          InkWell(
              onTap: () => sendMail(widget.product!.email!),
              child: buildDetails('email'.tr, widget.product!.email!)),
          SizedBox(height: 10),
          InkWell(
              onTap: () => _callPhone(widget.product!.phone!),
              child: buildDetails('tel'.tr, widget.product!.phone!)),
          SizedBox(height: 10),
          buildDetails('address'.tr, IsArabic ? widget.product!.translations![0].address
              : widget.product!.translations![1].address!),
          SizedBox(height: 20),
          Row(
            children: [
              Text("social".tr + ":",
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.grey.shade600,
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),
              SizedBox(width: 20),
            if(widget.product!.facebook!= "")  InkWell(
                onTap: () {
                  openSocial(
                      widget.product!.facebook!, widget.product!.facebook!);
                },
                child: Image.asset("assets/images/facebook.png",
                    width: 35, height: 35),
              ),
              SizedBox(width: 10),
              if(widget.product!.twitter!= "")  InkWell(
                onTap: () {
                  openSocial(
                      widget.product!.twitter!, widget.product!.twitter!);
                },
                child: Image.asset("assets/images/twitter.png",
                    width: 35, height: 35),
              ),
              SizedBox(width: 10),
              if(widget.product!.instagram!= "")  InkWell(
                onTap: () {
                  openSocial(
                      widget.product!.instagram!, widget.product!.instagram!);
                },
                child: Image.asset("assets/images/instagram.png",
                    width: 35, height: 35),
              ),
              SizedBox(width: 10),
              if(widget.product!.whatsapp!= "")  InkWell(
                onTap: () {
                  sendWhatsApp(widget.product!.whatsapp!);
                },
                child: Image.asset("assets/images/whatsapp.png",
                    width: 35, height: 35),
              ),
              SizedBox(width: 10),
            ],
          )
        ],
      ),
    );
  }

  buildDetails(title, subtitle) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 1,
            child: Text(title,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: Colors.grey.shade600,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
          ),
          Text( ":",
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: Colors.grey.shade600,
                  fontSize: 16,
                  fontWeight: FontWeight.bold)),
          SizedBox(width: 10),
          Expanded(
            flex: 2,
            child: Text(subtitle,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.grey.shade600, fontSize: 16)),
          ),
        ],
      ),
    );
  }

  void _callPhone(String phone) async {
    var url = "tel:$phone";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  sendMail(String email) {
    final Uri emailLaunchUri = Uri(
      scheme: 'mailto',
      path: email,
    );

    launch(emailLaunchUri.toString());
  }

  sendWhatsApp(String phone) async {
    String url = "";
    if (Platform.isAndroid) {
      // add the [https]
      url = "https://wa.me/$phone/?text=${Uri.parse(' ')}"; // new line
    } else {
      // add the [https]
      url =
          "https://api.whatsapp.com/send?phone=$phone=${Uri.parse(' ')}"; // new line
    }
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  openSocial(String url, String fallbackUrl) async {
    // Don't use canLaunch because of fbProtocolUrl (fb://)
    try {
      bool launched =
          await launch(url, forceSafariVC: false, forceWebView: false);
      if (!launched) {
        await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
      }
    } catch (e) {
      await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
    }
  }
}
