import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';

import '../../main.dart';
import 'map_page.dart';

class PlacesPage extends StatefulWidget {
  final ProductModel? product;

  const PlacesPage({Key? key, this.product}) : super(key: key);

  @override
  _PlacesPageState createState() => _PlacesPageState();
}

class _PlacesPageState extends State<PlacesPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
          itemBuilder: (context, index) {
            var place = widget.product!.place![index];
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: ListTile(
                onTap: () {
                  seeDialog(place);
                },
                title: Text(place.translations![IsArabic ?0 : 1].name!),
                trailing: Card(
                  color: Colors.black45,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(kRadius * 3)),
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(width: 6),
                        Text('show'.tr, style: TextStyle(color: Colors.white)),
                        SizedBox(width: 12),
                        Icon(CupertinoIcons.eye, color: Colors.white),
                        SizedBox(width: 6),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
          itemCount: widget.product!.place!.length),
    );
  }

  @override
  void initState() {
    super.initState();
    print("mahmoud ${widget.product!.place!.length}");
  }

  seeDialog(DetailsModel place) {
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Card(
              margin: EdgeInsets.all(26),
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12))),
              child: Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * .3,
                    child: ClassicGeneralDialogWidget(
                      titleText: 'googleMap'.tr,
                      contentText: 'openMap'.tr,
                      actions: [
                        Padding(
                          padding: EdgeInsets.all(16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                  onTap: () async {
                                    Get.to(MapPage(place: place));
                                  },
                                  child: Text("cancel".tr)),
                              SizedBox(width: 150),
                              InkWell(
                                  onTap: () async {
                                    final availableMaps =
                                        await MapLauncher.installedMaps;
                                    print(
                                        availableMaps); // [AvailableMap { mapName: Google Maps, mapType: google }, ...]

                                    await availableMaps.first.showMarker(
                                      coords: Coords(
                                          double.tryParse(place.lat!) ?? 0.0,
                                          double.tryParse(place.lng!) ?? 0.0),
                                      title: "",
                                    );
                                  },
                                  child: Text('ok'.tr)),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  IconButton(
                    onPressed: ()=>Navigator.pop(context),
                    icon: Icon(Icons.close),
                  )
                ],
              ),
            ),
          ],
        );
      },
      animationType: DialogTransitionType.size,
      curve: Curves.fastOutSlowIn,
      duration: Duration(seconds: 1),
    );
  }
}
