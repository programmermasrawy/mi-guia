import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/product_model.dart';
import 'package:photo_view/photo_view.dart';

import '../../main.dart';

class ZoomPhotos extends StatefulWidget {

  ZoomPhotos({Key? key}) : super(key: key);

  @override
  createState() => _ZoomPhotosState();
}

class _ZoomPhotosState extends State<ZoomPhotos> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    var _padding = EdgeInsets.all(16);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Get.back(),
        ),
      ),
      body: Container(
        child: BlocBuilder<HomeCubit, HomeState>(
            builder: (BuildContext context, state) {
          return ConditionalBuilder(
            condition: HomeCubit.get(context).images.isNotEmpty,
            builder: (BuildContext context) {
              return Swiper(
                scale: 1,
                fade: 0.6,
                pagination: SwiperPagination(
                    builder: new DotSwiperPaginationBuilder(
                        color: Colors.grey.shade300,
                        activeColor: PrimaryColor)),
                index: _currentIndex,
                onIndexChanged: (index){
                  setState(() {
                    _currentIndex = index;
                  });
                },
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) {
                  return PhotoView(
                    imageProvider: CachedNetworkImageProvider(
                          HomeCubit.get(context).images[index].image!,
                          maxHeight: height.toInt(),
                          maxWidth: width.toInt(),
                    ),
                  );
                },
                itemHeight: height,
                itemCount: HomeCubit.get(context).images.length,
                itemWidth: width,
              );
            },
            fallback: (BuildContext context) {
              return Center(
                child: Container(
                                          child: SizedBox(
                                            height: 50,
                                              width: 50,
                                              child:
                                                  Center(child:CircularProgressIndicator()))),
              );
            },
          );
        }),
      ),
    );
  }

}
