import 'package:cached_network_image/cached_network_image.dart';
import 'package:card_swiper/card_swiper.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/screens/folder_photos_screen/zoom_photos.dart';
import 'package:miguia/widgets/home/empty_widget.dart';

import '../../main.dart';

class FolderPhotosScreen extends StatefulWidget {
  DetailsModel? folder;

  FolderPhotosScreen({Key? key, this.folder}) : super(key: key);

  @override
  _FolderPhotosScreenState createState() => _FolderPhotosScreenState();
}

class _FolderPhotosScreenState extends State<FolderPhotosScreen> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    var _padding = EdgeInsets.all(16);
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.folder!.translations![IsArabic ?0 : 1].name!),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Get.back(),
        ),
      ),
      body: Container(
        padding: _padding,
        child: BlocBuilder<HomeCubit, HomeState>(
            builder: (BuildContext context, state) {
          return ConditionalBuilder(
            condition: !HomeCubit.get(context).loadImages,
            builder: (BuildContext context) {
              return  HomeCubit.get(context).images.isNotEmpty ?  ListView(
                children: [
                  SizedBox(height: height*.12),
                  Swiper(
                    scale: 1,
                    // pagination: SwiperPagination(
                    //     builder: new DotSwiperPaginationBuilder(
                    //         color: Colors.grey.shade300,
                    //         activeColor: PrimaryColor)),
                    viewportFraction: .9,
                    fade: 0.6,
                    index: _currentIndex,
                    onIndexChanged: (index){
                      setState(() {
                        _currentIndex = index;
                      });
                    },
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: (){
                          Get.to(ZoomPhotos());
                        },
                        child: Card(
                          elevation: 6,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(kRadius*3),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(kRadius*3),
                            child: CachedNetworkImage(
                             imageUrl: HomeCubit.get(context).images[index].image!,
                              fit: BoxFit.fill,
                              width: width *.8,
                              height: (height *.45 ) - (index *10),
                            ),
                          ),
                        ),
                      );
                    },
                    itemHeight: height*.4,
                    itemCount: HomeCubit.get(context).images.length,
                    itemWidth: width*.8,
                    layout: SwiperLayout.TINDER,
                  ),
                  SizedBox(height:20),
                  Text(widget.folder!.translations![IsArabic ?0 : 1].name!,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(
                          color: Colors.black,
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                          decoration:
                          TextDecoration.underline)),
                  SizedBox(height: 10),
                  Text(widget.folder!.translations![IsArabic ?0 : 1].description!),
                  SizedBox(height: 30),
                ],
              ):EmptyWidget();
            },
            fallback: (BuildContext context) {
              return Center(
                child: Container(
                                          child: SizedBox(
                                            height: 50,
                                              width: 50,
                                              child:
                                                  Center(child:CircularProgressIndicator()))),
              );
            },
          );
        }),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    HomeCubit.get(context).loadImages= true;
    HomeCubit.get(context).getFolderPhotos(context, widget.folder!.id);
  }
}
