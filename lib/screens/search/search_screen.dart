import 'package:cached_network_image/cached_network_image.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:map/map.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/settings/settings_cubit.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:miguia/screens/products/product_page.dart';
import 'package:miguia/screens/search/filter_screen.dart';

import '../../main.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  Widget appBarTitle = new Text(
    "Search Example",
    style: new TextStyle(color: Colors.white),
  );
  Icon icon = new Icon(
    Icons.search,
    color: PrimaryColor,
  );
  final globalKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _controller = new TextEditingController();
  List<dynamic> _list = [];
  bool? _isSearching;
  String _searchText = "";
  List searchresult = [];
  int areaId = -1;
  int categoryId = -1;
  _SearchListExampleState() {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _isSearching = false;
    SettingsCubit.get(context).searchResults.clear();
  }


  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    var _padding = EdgeInsets.all(16);
    /*24 is for notification bar on Android*/
    final double itemHeight = (height - kToolbarHeight - 24) / 5;
    final double itemWidth = width / 3;

    buildAppBar(BuildContext context) {
      return Card(
        borderOnForeground: true,
        child: Container(
          height: height * .08,
          child: Row(children: <Widget>[
            new IconButton(
              icon: icon,
              onPressed: () {
                setState(() {
                  if (this.icon.icon == Icons.search) {
                    this.icon = new Icon(
                      Icons.close,
                      color: PrimaryColor,
                    );
                    _handleSearchStart();
                  } else {
                    _handleSearchEnd();
                  }
                });
              },
            ),
            Expanded(
              child: TextFormField(
                controller: _controller,
                style: new TextStyle(
                  color: PrimaryColor,
                ),
                decoration: new InputDecoration(
                    hintText: "Search...",
                    hintStyle: new TextStyle(
                      color: PrimaryColor,
                    ),
                    border: InputBorder.none),
                onChanged: searchOperation,
              ),
            )
          ]),
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
          key: globalKey,
          appBar: AppBar(
            actions: [
              IconButton(
                icon: Icon(FontAwesomeIcons.filter),
                onPressed: () async {
                var result = await Get.to(TabBarPage(child: FilterScreen()),
                      popGesture: true, preventDuplicates: false);
                 print(result.toString());
                 // Fluttertoast.showToast(msg:result.toString());
                areaId = result[0];
                categoryId = result[1];
                setState(() {});
                SettingsCubit.get(context).search(data: _controller.text ,areaId: areaId!=-1 ? "$areaId" : "",catId: categoryId!=-1? "$categoryId":"");
                searchresult.clear();
                },
              )
            ],
          ),
          body: new Container(
            padding: EdgeInsets.all(16),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                buildAppBar(context),
                Flexible(
                  child: BlocBuilder<SettingsCubit, SettingsState>(
                      builder: (BuildContext context, state) {
                    return ConditionalBuilder(
                      condition:
                          SettingsCubit.get(context).searchResults.isNotEmpty,
                      builder: (BuildContext context) {
                        return GridView.count(
                            childAspectRatio: (itemWidth / itemHeight),
                            crossAxisCount: 3,
                            children: List.generate(
                                SettingsCubit.get(context).searchResults.length,
                                (index) {
                              var product = SettingsCubit.get(context)
                                  .searchResults[index];
                              return InkWell(
                                onTap: () {
                                  bool offer= false;
                                  String temp = product.visible!.firstWhere((element)
                                  => element.contains("offer"),orElse: ()=> "");
                                  if(temp.isNotEmpty)
                                    offer = true;
                                  Get.to(
                                      TabBarPage(
                                          product: product,
                                          showOffer: offer,
                                          child: ProductPage(
                                            product: product,
                                          )),
                                      preventDuplicates: false,
                                      popGesture: true);
                                },
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(6),
                                      child: ClipOval(
                                        child: Container(
                                          color: Color(0xff3FA9F5),
                                          child: Padding(
                                            padding: _padding,
                                            child: ClipOval(
                                              child: CachedNetworkImage(
                                                imageUrl: product.logo!,
                                                height: itemHeight / 2.2,
                                                placeholder: (context, url) => Container(
                                                    height: 50,
                                                    width: 50,
                                          child: SizedBox(
                                            height: 50,
                                              width: 50,
                                              child:
                                                  Center(child:Center(child:CircularProgressIndicator())))),
                                                errorWidget: (context, url, error) => Icon(Icons.error),
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Text(
                                      product.translations![IsArabic ? 0 : 1]
                                          .name!,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(color: Colors.black),
                                    )
                                  ],
                                ),
                              );
                            }));
                      },
                      fallback: (BuildContext context) {
                        return Center(
                          child: SizedBox(),
                        );
                      },
                    );
                  }),
                ),
              ],
            ),
          )),
    );
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.icon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Search Sample",
        style: new TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _controller.clear();
    });
  }

  void searchOperation(String searchText) {
    if(searchText.length >= 2) {
      SettingsCubit.get(context).search(data: searchText,areaId: areaId!=-1 ? "$areaId" : "",catId: categoryId!=-1? "$categoryId":"");
      searchresult.clear();
    }
    // if (_isSearching != null) {
    //   for (int i = 0; i < _list.length; i++) {
    //     String data = _list[i];
    //     if (data.toLowerCase().contains(searchText.toLowerCase())) {
    //       searchresult.add(data);
    //     }
    //   }
    // }
  }
}
