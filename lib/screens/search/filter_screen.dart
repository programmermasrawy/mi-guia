import 'package:auto_size_text/auto_size_text.dart';
import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:miguia/config/constants.dart';
import 'package:miguia/cubit/home/home_cubit.dart';
import 'package:miguia/cubit/settings/settings_cubit.dart';

import '../../main.dart';

class FilterScreen extends StatefulWidget {
  const FilterScreen({Key? key}) : super(key: key);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  int areaId = -1;
  int categoryId = -1;

  @override
  void initState() async {
    super.initState();
    HomeCubit.get(context).getHomeCategories(context);

    SettingsCubit.get(context).getAreas();
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var _padding = EdgeInsets.all(16);
    var theme = Theme.of(context).textTheme;
    return Scaffold(
        appBar: AppBar(
          actions: [
            InkWell(
              child: Center(
                child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 16),
                    child: Text(
                      "submit".tr,
                      style: TextStyle(color: Colors.white),
                    )),
              ),
              onTap: () => Get.back(result: [areaId, categoryId]),
            )
          ],
        ),
        body: Container(
          padding: _padding,
          height: height,
          width: width,
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text('areas'.tr + ":",
                    style: theme.bodyText1!.copyWith(
                        decoration: TextDecoration.underline, fontSize: 18)),
              ),
              BlocBuilder<SettingsCubit, SettingsState>(
                  builder: (BuildContext context, state) {
                return ConditionalBuilder(
                  condition: SettingsCubit.get(context).areas.isNotEmpty,
                  builder: (BuildContext context) {
                    return GridView.count(
                      crossAxisCount: 3,
                      childAspectRatio: 2.3,
                      physics: NeverScrollableScrollPhysics(),
                      // to disable GridView's scrolling
                      shrinkWrap: true,
                      // You won't see infinite size error
                      children: List.generate(
                          SettingsCubit.get(context).areas.length, (index) {
                        var category = SettingsCubit.get(context).areas[index];
                        return Padding(
                          padding: EdgeInsets.all(4.0),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                areaId = category.id!;
                              });
                            },
                            child: Card(
                              color: areaId == category.id
                                  ? PrimaryColor
                                  : Colors.grey.shade400,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(kRadius * 3)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.0, vertical: 2),
                                child: Center(
                                  child: AutoSizeText(
                                    category
                                        .translations![IsArabic ? 0 : 1].name!,
                                    minFontSize: 10,
                                    maxFontSize: 15,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  },
                  fallback: (BuildContext context) {
                    return Center(
                      child: Container(
                                          child: SizedBox(
                                            height: 50,
                                              width: 50,
                                              child:
                                                  Center(child:CircularProgressIndicator()))),
                    );
                  },
                );
              }),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text('category'.tr + ":",
                    style: theme.bodyText1!.copyWith(
                        decoration: TextDecoration.underline, fontSize: 18)),
              ),
              BlocBuilder<HomeCubit, HomeState>(
                  builder: (BuildContext context, state) {
                return ConditionalBuilder(
                  condition: HomeCubit.get(context).categories.isNotEmpty,
                  builder: (BuildContext context) {
                    return GridView.count(
                      crossAxisCount: 3,
                      childAspectRatio: 2.3,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      children: List.generate(
                          HomeCubit.get(context).categories.length, (index) {
                        var category = HomeCubit.get(context).categories[index];
                        return Padding(
                          padding: EdgeInsets.all(4.0),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                categoryId = category.id!;
                              });
                            },
                            child: Card(
                              color: categoryId == category.id
                                  ? PrimaryColor
                                  : Colors.grey.shade300,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(kRadius * 3)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 4.0, vertical: 2),
                                child: Center(
                                  child: AutoSizeText(
                                    category
                                        .translations![IsArabic ? 0 : 1].name!,
                                    minFontSize: 10,
                                    maxFontSize: 15,
                                    style: TextStyle(color:Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                    );
                  },
                  fallback: (BuildContext context) {
                    return Center(
                      child: Container(
                                          child: SizedBox(
                                            height: 50,
                                              width: 50,
                                              child:
                                                  Center(child:CircularProgressIndicator()))),
                    );
                  },
                );
              }),
            ],
          ),
        ));
  }
}
