import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:miguia/api/search_api.dart';
import 'package:miguia/api/settings_api.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/news_model.dart';
import 'package:miguia/data/product_model.dart';
import 'package:miguia/data/question_model.dart';

part 'settings_state.dart';

class SettingsCubit extends Cubit<SettingsState> {
  SettingsCubit() : super(SettingsInitial());
  static SettingsCubit get(context) => BlocProvider.of(context);
  List<NewsModel> news=[];
  List<CategoryModel> areas=[];
  List<ProductModel> searchResults = [];
  List<QuestionModel> questions=[];

  void fetchNews() async{
    news.clear();
    news = await SettingsApi().fetchNews();
    emit(newsLoaded());
  }
  void fetchQuestions() async{
    questions.clear();
    questions = await SettingsApi().fetchQuestions();
    emit(newsLoaded());
  }
  void getAreas() async{
    areas.clear();
    areas = await SearchApi().getAreasForSearch();
    emit(newsLoaded());
  }
  void search({String? catId:"",String? areaId:"",String? data:""}) async{
    searchResults.clear();
    searchResults = await SearchApi().search(data: data,areaId: areaId,catId: catId);
    emit(newsLoaded());
  }
}
