import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:miguia/api/home_api.dart';
import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/comment_model.dart';
import 'package:miguia/data/image_model.dart';
import 'package:miguia/data/news_model.dart';
import 'package:miguia/data/product_model.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  static HomeCubit get(context) => BlocProvider.of(context);

  List<CategoryModel> categories = [];
  List<CategoryModel> subCategories = [];
  List<ProductModel> products = [];
  List<ImageModel> images = [];
  bool loadImages = true;
  bool loadComments = true;
  bool generalLoading = true;
  List<NewsModel> sliders = [];
  List<NewsModel> banner = [];
  DetailsModel? place;
  ProductModel? productDetails;

  List<ProductModel> offers = [];
  List<CommentModel> comments = [];
  bool loadingOffers = true;
  List<ProductModel> related = [];

  getHomeCategories(context) async {
    categories = await HomeApi().getHomeCategories();
    print("categories" + categories.length.toString());
    emit(CategoriesLoaded());
  }

  getSliders(context) async {
    if (banner.isEmpty && sliders.isEmpty) {
      List<NewsModel> list = await HomeApi().getHomeSliders();

      list.forEach((element) {
        if (element.type == "banner") {
          banner.add(element);
        } else
          sliders.add(element);
      });
    }
    emit(CategoriesLoaded());
  }

  void getSubCategory(context, catId) async {
    subCategories.clear();
    subCategories = await HomeApi().getSubCategories(catId: catId);
    emit(CategoriesLoaded());
  }

  void getCategoryProducts(context, catId) async {
    products.clear();
    products = await HomeApi().getCategoryProducts(id: catId);
    emit(CategoriesLoaded());
  }

  void getFolderPhotos(context, folderId) async {
    images.clear();
    images = await HomeApi().getFolderImages(id: folderId);
    loadImages = false;
    emit(CategoriesLoaded());
  }

  void getRelated(context, id) async {
    related.clear();
    related = await HomeApi().getRelated(id: id);
    emit(CategoriesLoaded());
  }

  void getProductDetails(context, productId) async {
    productDetails = null;
    productDetails = await HomeApi().getProductDetails(id: productId);
    emit(CategoriesLoaded());
  }

  void getProductOffers(context, productId) async {
    offers.clear();
    offers = await HomeApi().getProductOffer(id: productId);
    loadingOffers = false;
    emit(CategoriesLoaded());
  }
  void getComments(context, faqId) async {
    comments.clear();
    comments = await HomeApi().getComments(id: faqId);
    loadComments = false;
    emit(CategoriesLoaded());
  }

  void getPlaceDetails(context, placeId) async {
    place = null;
    place = await HomeApi().getPlaceDetails(id: placeId);
    generalLoading = false;
    emit(CategoriesLoaded());
  }
}
