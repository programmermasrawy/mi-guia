
import 'package:miguia/data/category_model.dart';

class QuestionModel{
  int? id;
  String? image;
  String? created_at;
  int? comment_count;
  List<Translation>? translation;

  QuestionModel({ this.translation,this.id,this.image,this.comment_count,this.created_at});

  factory QuestionModel.fromMap(dynamic map) {
    if (null == map) return QuestionModel();
    var temp;
    return QuestionModel(
      id: map['id'] ?? 0,
      created_at: map['created_at'] ?? "",
      comment_count: map['comment_count'] ?? 0,
      image: map['image'] ?? "",
      translation: null == (temp = map['translate'])
          ? []
          : (temp is List
          ? temp.map((map) => Translation.fromMap(map)).toList()
          : []),
    );
  }
}