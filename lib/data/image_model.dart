

class ImageModel{
  String? image;

  ImageModel({this.image});

  factory ImageModel.fromMap(dynamic map) {
    if (null == map) return ImageModel();
    var temp;
    return ImageModel(
      image: map['image']?.toString(),
    );
  }
}