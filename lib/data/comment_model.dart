

class CommentModel{
  String? comment;
  String? added_at;

  CommentModel({this.comment,this.added_at});

  factory CommentModel.fromMap(dynamic map) {
    if (null == map) return CommentModel();
    var temp;
    return CommentModel(
      comment: map['comment'] ?? "",
      added_at: map['added_at'] ?? "",
    );
  }
}