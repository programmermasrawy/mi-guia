

class SettingsModel{
  List<SettingsTranslation>? data;
  SettingsModel({this.data});

  factory SettingsModel.fromMap(dynamic map) {
    if (null == map) return SettingsModel();
    var temp;
    return SettingsModel(
      data: null == (temp = map['translate'])
          ? []
          : (temp is List
          ? temp.map((map) => SettingsTranslation.fromMap(map)).toList()
          : []),
    );
  }

}

class SettingsTranslation{
  String? privacy_policy;
  String? about_us;

  SettingsTranslation({this.privacy_policy,this.about_us});

  factory SettingsTranslation.fromMap(dynamic map) {
    if (null == map) return SettingsTranslation();
    return SettingsTranslation(
      privacy_policy: map['privacy_policy']??"",
      about_us: map['about_us']??"",
    );
  }

}