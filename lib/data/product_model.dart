import 'package:miguia/data/category_model.dart';

class ProductModel {
  int? id;
  String? logo;
  String? website;
  String? email;
  String? phone;
  String? mobile;
  String? facebook;
  String? whatsapp;
  String? instagram;
  String? twitter;
  String? lat;
  String? lng;
  List<Translation>? translations;
  List<DetailsModel>? folder;
  List<DetailsModel>? place;
  List<DetailsModel>? career;
  List<String>? related;
  List<String>? visible;

  ProductModel(
      {this.id,
      this.logo,
      this.website,
      this.email,
      this.phone,
      this.mobile,
      this.facebook,
      this.whatsapp,
      this.instagram,
      this.twitter,
      this.lat,
      this.lng,
      this.translations,
      this.folder,
      this.related,
      this.visible,
      this.career,
      this.place});

  factory ProductModel.fromMap(dynamic map) {
    if (null == map) return ProductModel();
    var temp;
    return ProductModel(
      id: null == (temp = map['id'])
          ? null
          : (temp is num ? temp.toInt() : int.tryParse(temp)),
      logo: map['logo'] ?? map['photo'] ?? map['image'] ?? "",
      website: map['website'] != null ? map['website'] : "",
      email: map['email'] != null ? map['email'] : "",
      phone: map['phone'] != null ? map['phone'] : "",
      mobile: map['mobile'] != null ? map['mobile'] : "",
      facebook: map['facebook'] != null ? map['facebook'] : "",
      whatsapp: map['whatsapp'] != null ? map['whatsapp'] : "",
      instagram: map['instagram'] != null ? map['instagram'] : "",
      twitter: map['twitter'].toString(),
      lat: map['lat'].toString(),
      lng: map['lng'].toString(),
      translations: null == (temp = map['translate'])
          ? null == (temp = map['translation'])
              ? []
              : (temp is List
                  ? temp.map((map) => Translation.fromMap(map)).toList()
                  : [])
          : (temp is List
              ? temp.map((map) => Translation.fromMap(map)).toList()
              : []),
      folder: null == (temp = map['folder'])
          ? []
          : (temp is List
              ? temp.map((map) => DetailsModel.fromMap(map)).toList()
              : []),
      visible: null == (temp = map['visible'])
          ? null == (temp = map['visible'])
          ? []
          : (temp is List
          ? temp.map((map) => map.toString()).toList()
          : [])
          : (temp is List
          ? temp.map((map) => map.toString()).toList()
          : []),
      related: map['related'] != null && map['related'].toString() != "null"
          ? ["2"]
          : [],
      career: null == (temp = map['career'])
          ? []
          : (temp is List
              ? temp.map((map) => DetailsModel.fromMap(map)).toList()
              : []),
      place: null == (temp = map['place'])
          ? []
          : (temp is List
              ? temp.map((map) => DetailsModel.fromMap(map)).toList()
              : []),
    );
  }
}

class DetailsModel {
  int? id;
  String? lat;
  String? lng;
  String? photo;
  List<Translation>? translations;

  DetailsModel({this.id, this.translations, this.photo, this.lat, this.lng});

  factory DetailsModel.fromMap(dynamic map) {
    if (null == map) return DetailsModel();
    var temp;
    return DetailsModel(
        id: (map['id'] != null)
            ? null == (temp = map['id'])
                ? null
                : (temp is num ? temp.toInt() : int.tryParse(temp))
            : 0,
        lat: map['lat'] != null ? map['lat'].toString() : "",
        lng: map['lng'] != null ? map['lng'].toString() : "",
        photo: map['photo'] != null ? map['photo'].toString() : "",
        translations: map['translation'] != null
            ? List<Translation>.from(
                map['translation']!.map((data) => Translation.fromMap(data)))
            : map['translate']
                ? List<Translation>.from(
                    map['translate']!.map((data) => Translation.fromMap(data)))
                : []);
  }
}
