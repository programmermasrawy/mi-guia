import 'package:miguia/data/category_model.dart';
import 'package:miguia/data/image_model.dart';

class NewsModel {
  String? image;
  String? link;
  String? type;
  String? created_at;
  List<Translation>? translation;
  List<ImageModel>? images;

  NewsModel({this.image, this.link,this.images,this.created_at, this.type, this.translation});

  factory NewsModel.fromMap(dynamic map) {
    if (null == map) return NewsModel();
    var temp;
    return NewsModel(
      image: map['image'] ?? "",
      link: map['link'] ?? "",
      type: map['type'] ?? "",
      created_at: map['created_at'] ?? "",
      translation: null == (temp = map['translate'])
          ? []
          : (temp is List
              ? temp.map((map) => Translation.fromMap(map)).toList()
              : []),
      images: null == (temp = map['images'])
          ? []
          : (temp is List
              ? temp.map((map) => ImageModel.fromMap(map)).toList()
              : []),
    );
  }
}
