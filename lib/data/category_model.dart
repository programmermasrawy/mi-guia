import 'package:miguia/data/product_model.dart';

class CategoryModel {
  int? id;
  String? image;
  bool? sub;
  String? identify;
  List<ProductModel>? products;
  List<Translation>? translations;

  CategoryModel(
      {this.id,
      this.image,
      this.identify,
      this.products,
      this.sub,
      this.translations});

  factory CategoryModel.fromMap(dynamic map) {
    if (null == map) return CategoryModel();
    var temp;
    return CategoryModel(
      id: null == (temp = map['id'])
          ? null
          : (temp is num ? temp.toInt() : int.tryParse(temp)),
      image: map['image'] ?? "",
      identify: map['identify'] != null ? map['identify'] : "",
      sub: null == (temp = map['sub'])
          ? null
          : (temp is bool
              ? temp
              : (temp is num
                  ? 0 != temp.toInt()
                  : ('true' == temp.toString()))),
      translations: null == (temp = map['translate'])
          ? []
          : (temp is List
              ? temp.map((map) => Translation.fromMap(map)).toList()
              : []),
      products: null == (temp = map['product'])
          ? []
          : (temp is List
              ? temp.map((map) => ProductModel.fromMap(map)).toList()
              : []),
    );
  }
}

class Translation {
  String? name;
  String? locale;
  String? description;
  String? address;

  Translation({this.name, this.locale,this.address, this.description});

  factory Translation.fromMap(dynamic map) {
    if (null == map) return Translation();
    var temp;
    return Translation(
      name: map['name'] ?? "",
      locale: map['locale'] ?? "",
      description: map['description'] ?? "",
      address: map['address'] ?? "",
    );
  }
}
