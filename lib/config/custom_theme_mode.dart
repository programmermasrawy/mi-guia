import 'package:flutter/material.dart';

import 'constants.dart';

/// Responsible of handling themes.
class CustomThemeMode {
  static light(BuildContext context) {
    return ThemeData(
        fontFamily: "ArabicFont",
        appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(color: Colors.white),
          elevation: 0,
          titleTextStyle: TextStyle(color:Colors.white,fontSize: 18,fontWeight: FontWeight.w600),
          centerTitle: true,
        ),
        progressIndicatorTheme: ProgressIndicatorThemeData(
              color: Colors.grey.shade300,
          circularTrackColor: PrimaryColor,
        ),
        dialogTheme: DialogTheme(
          titleTextStyle: TextStyle(
              color: Colors.black87, fontSize: 22, fontWeight: FontWeight.w700),
          contentTextStyle:  TextStyle(
              color: PrimaryColor, fontSize: 16, fontWeight: FontWeight.w700),
        ),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
              minimumSize: Size(double.infinity, 50),
              textStyle: TextStyle(color: Colors.white),
              backgroundColor: PrimaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kCardRadius * 2))),
        ),
        textTheme: TextTheme(
            headline1: TextStyle(
                color: PrimaryColor, fontSize: 22, fontWeight: FontWeight.w700),
            headline2: TextStyle(
                color: PrimaryColor, fontSize: 22, fontWeight: FontWeight.w600),
            headline3: TextStyle(
                color: PrimaryColor, fontSize: 22, fontWeight: FontWeight.w500),
            headline4: TextStyle(
                color: PrimaryColor, fontSize: 22, fontWeight: FontWeight.w400),
            headline5: TextStyle(
                color: PrimaryColor, fontSize: 22, fontWeight: FontWeight.w300),
            bodyText1: TextStyle(
                color: PrimaryColor, fontSize: 16, fontWeight: FontWeight.w500),
            bodyText2: TextStyle(
                color: PrimaryColor, fontSize: 16, fontWeight: FontWeight.w400),
            subtitle1: TextStyle(
                color: Color(0xff6A6A6A), fontSize: 14, fontWeight: FontWeight.w500),
            subtitle2: TextStyle(
                color: Color(0xff6A6A6A), fontSize: 14, fontWeight: FontWeight.w400),
            button: TextStyle(
                color: PrimaryColor, fontSize: 16, fontWeight: FontWeight.w700)
        ),
        cardTheme: CardTheme(
          color: Colors.white,
          elevation: 8,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kCardRadius),
          ),
        ),
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(kCardRadius),
          ),
        ),
        primarySwatch: MaterialColor(PrimaryColor.value, {
          50: PrimaryColor.withOpacity(0.1),
          100: PrimaryColor.withOpacity(0.2),
          200: PrimaryColor.withOpacity(0.3),
          300: PrimaryColor.withOpacity(0.4),
          400: PrimaryColor.withOpacity(0.5),
          500: PrimaryColor.withOpacity(0.6),
          600: PrimaryColor.withOpacity(0.7),
          700: PrimaryColor.withOpacity(0.8),
          800: PrimaryColor.withOpacity(0.9),
          900: PrimaryColor.withOpacity(1.0),
        }),
        iconTheme: IconThemeData(color: Colors.black),
        primaryColor: PrimaryColor,
        scaffoldBackgroundColor: Colors.white);
  }
}
