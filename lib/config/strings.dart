class Strings {
  static const String appName = "Mi GUIA";

  static const String readMore = "إقرأ المزيد";
  static const String offers = "العروض والتخفيضات";
  static const String home = "الرئيسية";
  static const String share = "مشاركة";
  static const String special_offer = "عروض خاصة";
  static const String qAnda = "س&ج";
  static const String settings = "الإعدادات";
  static const String next = "التالي";
  static const String toHome = "إلي الرئيسية";
  static const String skip = "تخطِ";
  static const String intro = "Mall Sanitary Ware in One App";
  static const String subIntro = "Everything that you need to"
      " know about Field sanitary is now here."
      " All the information in one app";
  static String services = "الخدمات";

  static String engOffice = "المكاتب الهندسية";

  static String factorys = "المصانع";

  static String technical = "خدمة العملاء";

  static String localMarket = "السوق";
}
