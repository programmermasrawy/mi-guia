import 'package:flutter/material.dart';

const Color PrimaryColor = Color(0xff3fa9f5);
const Color LightWeightPrimaryColor = Color(0xfffe014a);

const Gradient kGradient = LinearGradient(colors: <Color>
[LightWeightPrimaryColor, PrimaryColor], stops: [0.25, 0.75]);
const double kCardRadius = 10.0;
const kRadius = Radius.circular(10);
const double kButtonPadding = 12.0;
const double Space = 12.0;
const double kButtonFontSize = 16.0;
const WhiteTextStyle = TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.w600);
const smallWhiteTextStyle = TextStyle(color: Colors.white,fontSize: 14);
