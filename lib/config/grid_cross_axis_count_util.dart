class ResponsiveGridUtils{
  static int getCrossAxisCount(double maxWidth) {
    if(maxWidth >= 0 && maxWidth <= 599)
      return 2;
    else if(maxWidth >= 600 && maxWidth <= 904)
      return 4;
    else if(maxWidth >= 905)
      return 6;
    return 1;
  }
}