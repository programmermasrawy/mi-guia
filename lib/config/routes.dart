import 'package:miguia/config/strings.dart';
import 'package:flutter/cupertino.dart';

import '../main.dart';

class Routes {
  static Map<String, WidgetBuilder> getRoutes() {
    return {
      HOME_PAGE_ROUTE : (context) => StartPage(title: Strings.appName),

    };
  }

  static const String HOME_PAGE_ROUTE = "/";
}