import 'package:card_swiper/card_swiper.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:miguia/cubit/settings/settings_cubit.dart';
import 'package:miguia/screens/home/tab_bar_page.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:responsive_framework/utils/scroll_behavior.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'config/constants.dart';
import 'config/custom_theme_mode.dart';
import 'config/local_string.dart';
import 'config/routes.dart';
import 'cubit/home/home_cubit.dart';

Locale mainLocale = Locale("ar", "EG");
bool IsArabic = true;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await getLocale();
  // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  //   statusBarColor: Colors.transparent, // transparent status bar
  // ));
  runApp(MyApp());
}

getLocale() async {
  final shared = await SharedPreferences.getInstance();
  var lang = shared.getString('lang');
  if (lang != null && lang.isNotEmpty) {
    if (lang == "En") {
      mainLocale = Locale("en", "US");
      IsArabic = false;
    }
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => HomeCubit(),
        ),
        BlocProvider(
          create: (_) => SettingsCubit(),
        ),
      ],
      child: GetMaterialApp(
        title: 'appName'.tr,
        debugShowCheckedModeBanner: false,
        theme: CustomThemeMode.light(context),
        locale: mainLocale,
        translations: LocaleString(),
        supportedLocales: [
          Locale("ar", "EG"),
          Locale("en", "US"),
        ],
        localizationsDelegates: [
          CountryLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        builder: (context, widget) => ResponsiveWrapper.builder(
          BouncingScrollWrapper.builder(context, widget!),
          maxWidth: 1200,
          minWidth: 430,
          defaultScale: true,
          breakpoints: [
            ResponsiveBreakpoint.resize(420, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.autoScale(1000, name: TABLET),
            ResponsiveBreakpoint.resize(1200, name: DESKTOP),
            ResponsiveBreakpoint.autoScale(2460, name: "4K"),
          ],
        ),
        initialRoute: Routes.HOME_PAGE_ROUTE,
        routes: Routes.getRoutes(),
      ),
    );
  }
}

class StartPage extends StatefulWidget {
  StartPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  bool _seen = false;

  @override
  void initState() {
    super.initState();
    checkFirstSeen();
  }

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _seen = (prefs.getBool('seen') ?? false);
    Future.delayed(Duration(seconds: 2), () {
      if (_seen)
        Get.offAll(TabBarPage());
      else {
      Get.offAll(LanguagePage());
      prefs.setBool('seen', true);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Container(
      color: PrimaryColor,
      child: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            "assets/images/logo.png",
            width: width * .6,
            height: height * .25,
            fit: BoxFit.fill,
          ),
          // Text(
          //   "intro".tr,
          //   style: WhiteTextStyle,
          //   textAlign: TextAlign.center,
          // ),
          // Container(
          //     padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
          //     child: Text(
          //       'subIntro'.tr,
          //       maxLines: 4,
          //       style: smallWhiteTextStyle,
          //       overflow: TextOverflow.ellipsis,
          //       textAlign: TextAlign.center,
          //     )),
        ],
      )),
    ));
  }
}

class OnBoardingPage extends StatelessWidget {
  var _swiperCtrl = new SwiperController();

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    Widget _buildSplash(
        {String? image,
        String? buttonTxt,
        onPressed,
        String? subscription: ""}) {
      return Container(
          height: height,
          width: width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.only(bottomRight: kRadius * 4),
                      child: Image.asset(
                        "assets/images/img.png",
                        width: width,
                        height: height * .65,
                        fit: BoxFit.fill,
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                          padding: EdgeInsets.all(60),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: height * .05),
                              Image.asset(
                                image!,
                                width: width * 4,
                                height: height * .25,
                              ),
                              SizedBox(height: height * .1),
                              Text(
                                subscription!,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          )),
                    ),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.all(60),
                        child: InkWell(
                            onTap: () {
                              Get.offAll(TabBarPage());
                            },
                            child: Text(
                              'skip'.tr.toUpperCase(),
                              style: TextStyle(
                                  color: PrimaryColor,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700),
                            )),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                  child: Text(
                   '',
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  )),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        minimumSize: Size(width, 60),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(topLeft: kRadius * 4),
                        )),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                        Text(buttonTxt!, style: TextStyle(color: Colors.white)),
                        SizedBox(),
                      ],
                    ),
                    onPressed: () => onPressed(),
                  ))
            ],
          ));
    }

    return Scaffold(
      body: Swiper(
        controller: _swiperCtrl,
        scrollDirection: Axis.horizontal,
        pagination: SwiperPagination(
            // alignment: Alignment.bottomCenter,
            margin: EdgeInsets.only(bottom: height * .38),
            builder: DotSwiperPaginationBuilder(
              space: 7,
              activeColor: Colors.white,
              color: Colors.black12,
            )),
        itemBuilder: (BuildContext context, int index) {
          return index == 0
              ? _buildSplash(
                  image: "assets/images/intro1.png",
                  buttonTxt: 'next'.tr,
                  subscription: 'factory'.tr,
                  onPressed: () {
                    _swiperCtrl.move(++index);
                  })
              : index == 1
                  ? _buildSplash(
                      image: "assets/images/intro2.png",
                      buttonTxt: 'next'.tr,
                      subscription: 'eng_office'.tr,
                      onPressed: () {
                        _swiperCtrl.move(++index);
                      })
                  : index == 2
                      ? _buildSplash(
                          image: "assets/images/intro3.png",
                          buttonTxt: 'next'.tr,
                          subscription: 'market'.tr,
                          onPressed: () {
                            _swiperCtrl.move(++index);
                          })
                      : index == 3
                          ? _buildSplash(
                              image: "assets/images/intro4.png",
                              buttonTxt: 'toHome'.tr,
                              subscription: 'Technical'.tr,
                              onPressed: () {
                                _swiperCtrl.move(++index);
                              })
                          : Container(
                              color: PrimaryColor,
                              child: Center(
                                  child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Image.asset(
                                    "assets/images/logo.png",
                                    width: width * .6,
                                    height: height * .25,
                                    fit: BoxFit.fill,
                                  ),
                                  SizedBox(height: height * .1),
                                  InkWell(
                                    onTap: () => Get.offAll(TabBarPage()),
                                    child: Text(
                                      'toHome'.tr,
                                      style: WhiteTextStyle,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              )));
        },
        itemCount: 5,
        viewportFraction: 1,
      ),
    );
  }
}

class LanguagePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var btStyle = TextButton.styleFrom(
        backgroundColor: Colors.blue.shade700,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(kRadius*3)
        )
    );
    return Scaffold(
        backgroundColor: PrimaryColor,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/logo.png",
              width: width * .6,
              height: height * .25,
              fit: BoxFit.fill,
            ),
            Text(
              "superstore".tr,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: Colors.white,
                fontSize: 22
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: height *.08,
            ),
            Container(
                width: width,
                height: 58,
                padding: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: width *.25,
                      child: TextButton(
                        onPressed: () async {
                          final shared = await SharedPreferences.getInstance();
                          shared.setString('lang', 'ar');
                          Get.updateLocale(Locale("ar", "EG"));
                          Get.offAll(OnBoardingPage());
                        },
                        style: btStyle,
                        child: Text(
                          "العربية",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(width: width *.1),
                    SizedBox(
                      width: width *.25,
                      child: TextButton(
                        onPressed: () async {
                          final shared = await SharedPreferences.getInstance();
                          shared.setString('lang', 'En');
                          Get.updateLocale(Locale("en", "US"));
                          Get.offAll(OnBoardingPage());
                        },
                        style: btStyle,
                        child: Text("English",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                )),
          ],
        ));
  }
}
